﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using Serilog;
using System.Text;

namespace ViverBot.Logging
{
    /// <summary>
    ///     Methods for standardized logging.
    /// </summary>
    public static class ViverLog
    {

        public static void Information(string messageTemplate)
        {
            Log.Information(messageTemplate);
        }

        public static void Information(string messageTemplate, params object?[]? propertyValues)
        {
            Log.Information(messageTemplate, propertyValues);
        }

        public static void Warning(string messageTemplate)
        {
            Log.Warning(messageTemplate);
        }

        public static void Warning(string messageTemplate, params object?[]? propertyValues)
        {
            Log.Warning(messageTemplate, propertyValues);
        }

        public static void Error(string messageTemplate)
        {
            Log.Error(messageTemplate);
        }

        public static void Error(string messageTemplate, params object?[]? propertyValues)
        {
            Log.Error(messageTemplate, propertyValues);
        }

        /// <summary>
        ///     Logs that the client connected to Discord.<br></br>
        ///     INFORMATION
        /// </summary>
        /// <param name="client">
        ///     The DiscordClient.
        /// </param>
        public static void ClientConnected(DiscordClient client)
        {
            Log.Information(
                "Connected as {Username}. Status: {Status}",
                client.CurrentUser.Username,
                client.CurrentUser.Presence.Status.GetName());
        }

        /// <summary>
        ///     Logs that the activity was updated.<br></br>
        ///     INFORMATION
        /// </summary>
        /// <param name="client">
        ///     <inheritdoc cref="ClientConnected(DiscordClient)" path="/param[@name='client']"/>
        /// </param>
        /// <param name="member">
        ///     The <see cref="DiscordMember"/> who executed the interaction.
        /// </param>
        public static void ActivityUpdated(DiscordClient client, DiscordMember member)
        {
            Log.Information("Activity was updated.\n[User] {User} (Id: {Id})\n{Type}\n{Activity}\n{Status}",
                member.Username,
                member.Id,
                client.CurrentUser.Presence.Activity.ActivityType.ToString(),
                client.CurrentUser.Presence.Activity.Name,
                client.CurrentUser.Presence.Status.ToString());
        }

        /// <summary>
        ///     Logs that the status was updated.<br></br>
        ///     INFORMATION
        /// </summary>
        /// <param name="client">
        ///     <inheritdoc cref="ClientConnected(DiscordClient)" path="/param[@name='client']"/>
        /// </param>
        /// <param name="member">
        ///     <inheritdoc cref="ActivityUpdated(DiscordClient, DiscordMember)" path="/param[@name='member']"/>
        /// </param>
        public static void StatusUpdated(DiscordClient client, DiscordMember member)
        {
            Log.Information("Status was updated.\n[User] {User} (Id: {Id})\n[Status] {Status}",
                member.Username,
                member.Id,
                client.CurrentUser.Presence.Status.ToString());
        }

        /// <summary>
        ///     Logs that the bot will reconnect.<br></br>
        ///     INFORMATION
        /// </summary>
        /// <param name="member">
        ///     <inheritdoc cref="ActivityUpdated(DiscordClient, DiscordMember)" path="/param[@name='member']"/>
        /// </param>
        public static void ReconnectRequested(DiscordMember member)
        {
            Log.Information("A manual reconnect was performed.\n[User] {User} (Id: {Id})",
                member.Username,
                member.Id);
        }

        public static void FanficMessageDeleted(DiscordMessage message, string reason)
        {
            Log.Information("Fanfic message was deleted.\n[Content] {Content}\n[Reason] {Reason}",
                message.Content,
                reason);
        }

        /// <summary>
        ///     Logs failed commands. These are expected.<br></br>
        ///     WARNING
        /// </summary>
        /// <param name="context">
        ///     The InteractionContext containing information about the command.
        /// </param>
        /// <param name="reason">
        ///     Currently only a error code.
        /// </param>
        public static void CommandFailed(InteractionContext context, string? reason = null)
        {
            var data = LogCommandData(context);
            if (reason is not null)
            {
                data += $"\n[Reason] {reason}";
            }

            Log.Warning(
                "Command {CommandName} failed!\n{Data}",
                context.CommandName,
                data);
        }

        /// <summary>
        ///     Logs errored commands. These are unexpected.<br></br>
        ///     ERROR
        /// </summary>
        /// <param name="context">
        ///     The InteractionContext containing information about the command.
        /// </param>
        /// <param name="exception">
        ///     The exception message.
        /// </param>
        public static void CommandErrored(InteractionContext context, string? exception = null)
        {
            var data = LogCommandData(context);
            if (exception is not null)
            {
                data += $"\n[Exception] {exception}";
            }

            Log.Error(
                "Command {CommandName} errored!\n{Data}",
                context.CommandName,
                data);
        }

        public static void SubscribeToThreadErrored(string exception, ulong threadId)
        {
            Log.Error(
                $"Unable to subscribe to thread: {threadId}\n" +
                $"[Exception] {exception}");
        }

        public static void UnsubscribeFromThreadErrored(string exception, ulong threadId)
        {
            Log.Error(
                $"Unable to unsubscribe from thread: {threadId}\n" +
                $"[Exception] {exception}");
        }

        public static void HandlerRestoreThreadErrored(string exception, ulong threadId)
        {
            Log.Error(
                $"Unable to restore event handler for thread: {threadId}\n" +
                $"[Exception] {exception}");
        }

        public static void SubscribeToDmHelpErrored(string exception, int dmHelpKeyId)
        {
            Log.Error(
                $"Unable to subscribe to dmHelpKey: {dmHelpKeyId}\n" +
                $"[Exception] {exception}");
        }

        public static void UnsubscribeFromDmHelpErrored(string exception, int dmHelpKeyId)
        {
            Log.Error(
                $"Unable to unsubscribe from dmHelpKey: {dmHelpKeyId}\n" +
                $"[Exception] {exception}");
        }

        public static void HandlerRestoreDmHelpErrored(string exception, int dmHelpKeyId)
        {
            Log.Error(
                $"Unable to restore event handler for dmHelpKey: {dmHelpKeyId}\n" +
                $"[Exception] {exception}");
        }

        /// <summary>
        ///     Builds a standard log object containing information about the command.
        /// </summary>
        /// <param name="context">
        ///     The InteractionContext containing information about the command.
        /// </param>
        /// <returns>
        ///     A formatted string containing information about the command.
        /// </returns>
        private static string LogCommandData(InteractionContext context)
        {
            var sb = new StringBuilder();
            // Guild is null when the bot receives a DM
            if (context.Guild is not null)
            {
                sb.AppendLine($"[Guild] {context.Guild.Name}");
                sb.AppendLine($"[Channel] {context.Channel.Name}");
                sb.AppendLine($"[Member] {context.Member.Username}");
            }
            else
            {
                sb.AppendLine($"[Channel] Direct Message");
            }

            sb.Append($"[Command] {context.CommandName} ");
            if (context.Interaction.Data.Options is not null)
            {
                foreach (var option in context.Interaction.Data.Options)
                {
                    sb.Append(option.Options.FirstOrDefault() is null
                        ? option.Name
                        : $"{option.Name}: {option.Options.First().Value}");
                }
            }

            return sb.ToString();
        }
    }
}
