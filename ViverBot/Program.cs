﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using ViverBot.Extensions;
using Serilog;

namespace ViverBot
{
    internal class Program
    {
        private async static Task Main(string[] args)
        {
            try
            {
                await CreateHostBuilder(args).RunConsoleAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ended with: " + ex.Message);
            }
            finally
            {

            }
        }

        /// <summary>
        ///     Creates and configures the host.
        /// </summary>
        /// <param name="args">
        ///     The command line args.
        /// </param>
        /// <returns>
        ///     The initialized host.
        /// </returns>
        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseConsoleLifetime()
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureAppConfiguration((hostingContext, configuration) =>
                {
                    configuration.LoadConfiguration();
                });
        }
    }
}