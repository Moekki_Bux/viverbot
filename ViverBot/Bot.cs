﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using DSharpPlus.SlashCommands.EventArgs;
using Microsoft.EntityFrameworkCore;
using Serilog;
using ViverBot.Attributes;
using ViverBot.Context;
using ViverBot.Extensions;
using ViverBot.Logging;
using ViverBot.Models;
using ViverBot.Modules;
using ViverBot.Services;
using ViverBot.Services.AutoReactServices;
using ViverBot.Services.DmHelpServices;
using ViverBot.Services.FanficServices;

namespace ViverBot
{
    /// <summary>
    ///     A container for the bots most important things. It's the bot, okay?
    /// </summary>
    public sealed partial class Bot : IHostedService
    {
        public DiscordClient Client { get; set; }
        public InteractivityExtension Interactivity { get; set; }
        public SlashCommandsExtension Commands { get; set; }

        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _serviceProvider;
        private readonly IHostApplicationLifetime _applicationLifetime;

        /// <summary>
        ///     Initializes a new instance of the bot with a configuration.
        /// </summary>
        /// <param name="applicationLifetime">
        ///     The programs lifetime.
        /// </param>
        /// <param name="configuration">
        ///     The configuration.
        /// </param>
        public Bot(IHostApplicationLifetime applicationLifetime, IConfiguration configuration)
        {
            _configuration = configuration;

            // Configure the logger
            var appPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var logPath = Path.Combine(appPath, "viverbot", "logs");
            Directory.CreateDirectory(logPath);
            var loggerFactory = new LoggerFactory().AddSerilog();
            var outputTemplate = "[{Timestamp:yyyy-MM-dd HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}";
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Async(l => l.File(
                    path: Path.Combine(logPath, "viver.log"),
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: outputTemplate))
                .WriteTo.Async(l => l.Console(
                    outputTemplate: outputTemplate))
                .CreateLogger();

            Console.WriteLine("Application started: Press Ctrl+C to shut down.");

            _applicationLifetime = applicationLifetime;

            var services = new ServiceCollection();
            services.AddDbContext<ViverContext>(options =>
            {
                options.UseMySql(_configuration["connectionString"]!);
                options.UseLoggerFactory(loggerFactory);
            });

            // Add the settings to DI
            services.AddOptions<ViverSettings>()
                .Bind(_configuration);

            // Add services here
            services.AddSingleton<FanficObserver>();
            services.AddSingleton<AutoReactObserver>();
            services.AddSingleton<DmHelpObserver>();
            services.AddScoped<IResponseService, ResponseService>();
            services.AddScoped<IFanficService, FanficService>();
            services.AddScoped<IAlastorService, AlastorService>();
            services.AddScoped<IAutoReactService, AutoReactService>();
            services.AddScoped<IDmHelpService, DmHelpService>();

            _serviceProvider = services.BuildServiceProvider();

            // Configure the bot with its token and intents
            // The GuildMembers intent needs to be enabled on the applications page
            var dcConfig = new DiscordConfiguration()
            {
                Token = _configuration["token"],
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.All,
                AutoReconnect = true,
                LoggerFactory = loggerFactory,
                MinimumLogLevel = _configuration["logLevel"]!.ToLogLevel(),
                LogUnknownEvents = false
            };

            Client = new DiscordClient(dcConfig);

            Interactivity = Client.UseInteractivity(
                new InteractivityConfiguration()
                {
                    Timeout = TimeSpan.FromMinutes(_configuration.GetValue<int>("timeout"))
                });

            Commands = Client.UseSlashCommands(
                new SlashCommandsConfiguration()
                {
                    Services = _serviceProvider
                });

            ulong supportServerId = _configuration.GetValue<ulong>("supportServerId");

            // Register command classes here
            Commands.RegisterCommands<CoreModule>();
            Commands.RegisterCommands<FanficThreadModule>();
            Commands.RegisterCommands<AutoReactModule>();
            Commands.RegisterCommands<DmHelpModule>();
            //Commands.RegisterCommands<AlastorModule>();

            Commands.RegisterCommands<ControlModule>(supportServerId);
            Commands.RegisterCommands<DebugModule>(supportServerId);

            // Register events here
            Commands.SlashCommandErrored += Commands_SlashCommandErrored;
            Client.Ready += Client_Ready;
            Client.GuildDownloadCompleted += Client_GuildDownloadCompleted;
        }

        /// <summary>
        ///     Handler for when a slash command errors. Just like the name suggests. Convenient.
        /// </summary>
        /// <param name="sender">
        ///     The SlashCommandsExtension.
        /// </param>
        /// <param name="e">
        ///     The SlashCommandErrorEventArgs. Contains everything interesting.
        /// </param>
        /// <returns></returns>
        private async Task Commands_SlashCommandErrored(SlashCommandsExtension sender, SlashCommandErrorEventArgs e)
        {
            if (e.Exception is SlashExecutionChecksFailedException slex)
            {
                foreach (var check in slex.FailedChecks)
                {
                    if (check is SlashCooldownAttribute scat)
                    {
                        var reset = DateTimeOffset.UtcNow.AddSeconds(scat.Reset.TotalSeconds).ToUnixTimeSeconds();
                        await e.Context.CreateResponseAsync("The command cooldown expires <t:" + reset + ":R>", true);
                    }
                    else if (check is SlashRequireThreadAttribute)
                    {
                        var str = "This command is only usable within a thread.\n" +
                            "https://tenor.com/bhS5e.gif";
                        await e.Context.CreateResponseAsync(str, true);
                    }
                }

                return;
            }

            ViverLog.CommandErrored(e.Context, e.Exception.Message);
            var owner = Client.CurrentApplication.Owners.First();
            await e.Context.Channel.SendMessageAsync("If you see this please contact " +
                owner.Mention + " and provide as many information about the error as you can.");
        }


        private Task Client_GuildDownloadCompleted(DiscordClient sender, GuildDownloadCompletedEventArgs args)
        {
            ViverLog.Information("Client_GuildDownloadCompleted");

            _ = Task.Run(MigrateDatabase);
            return Task.CompletedTask;
        }

        private Task Client_Ready(DiscordClient sender, ReadyEventArgs args)
        {
            ViverLog.Information("Client_Ready");
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Creates and updates the database if needed. Also calls <see cref="RestoreFanficHandlers(Fanfic[])"/>
        ///     to restore event handlers.
        /// </summary>
        /// <returns></returns>
        private async Task MigrateDatabase()
        {
            var dbContext = _serviceProvider.GetRequiredService<ViverContext>();

            await dbContext.Database.MigrateAsync();

            var fanfics = await dbContext.Fanfics
                .Where(s => s.IsActive)
                .ToArrayAsync();
            await RestoreFanficHandlers(fanfics);

            var autoReacts = await dbContext.AutoReactChannels
                .ToArrayAsync();
            await RestoreAutoReacts(autoReacts);

            var dmHelpKeys = await dbContext.DmHelpKeys
                .Include(k => k.DmHelpResponse)
                .ToArrayAsync();
            await RestoreDmHelpHandlers(dmHelpKeys);

            await dbContext.SaveChangesAsync();
        }

        /// <summary>
        ///     Restores event handlers for active fanfics.
        /// </summary>
        /// <param name="fanfics">
        ///     All fanfics for which event handlers should be restored.
        /// </param>
        /// <returns></returns>
        private async Task RestoreFanficHandlers(Fanfic[] fanfics)
        {
            var fanficObserver = _serviceProvider.GetRequiredService<FanficObserver>();
            foreach (var fanfic in fanfics)
            {
                if (await Client.DoesChannelExistAsync(fanfic.ThreadId))
                {
                    var thread = (DiscordThreadChannel)await Client.GetChannelAsync(fanfic.ThreadId);
                    if (fanficObserver.Subscribe(Client, thread))
                    {
                        if (fanfic.LockMessageId is not null)
                        {
                            var msg = await thread.GetMessageAsync((ulong)fanfic.LockMessageId);
                            await msg.DeleteAsync("Maintenance downtime done");
                            fanfic.LockMessageId = null;

                            await thread.ModifyAsync(t => t.Locked = false);
                        }
                        else
                        {
                            ViverLog.HandlerRestoreThreadErrored("LockMessageId is not set!", fanfic.ThreadId);
                        }
                    }
                    else
                    {
                        fanfic.IsActive = false;
                    }
                }
                else
                {
                    ViverLog.HandlerRestoreThreadErrored("Thread channel was not found!", fanfic.ThreadId);
                }
            }

            var handlerCount = fanficObserver.GetCountOfObservers();
            ViverLog.Information("{HandlerCount} fanfic handlers were restored.", handlerCount);
        }

        private async Task RestoreAutoReacts(AutoReactChannel[] autoReacts)
        {
            var autoReactObserver = _serviceProvider.GetRequiredService<AutoReactObserver>();
            foreach (var autoReact in autoReacts)
            {
                if (await Client.DoesChannelExistAsync(autoReact.ChannelId))
                {
                    var channel = await Client.GetChannelAsync(autoReact.ChannelId);
                    var reactions = autoReact.Emojis.Replace(" ", "").Split(',').ToList();
                    if (!autoReactObserver.Subscribe(Client, channel, reactions))
                    {
                        ViverLog.Warning("autoReact subscription at startup failed!");
                    }
                }
                else
                {
                    ViverLog.Warning("The autoReact channel with id {Id} doesn't exist!", autoReact.ChannelId);
                }
            }
        }

        private async Task RestoreDmHelpHandlers(DmHelpKey[] dmHelpKeys)
        {
            var dmHelpObserver = _serviceProvider.GetRequiredService<DmHelpObserver>();
            foreach (var dmHelpKey in dmHelpKeys)
            {
                var guild = await Client.GetGuildAsync(dmHelpKey.GuildId);
                if (!dmHelpObserver.Subscribe(Client, guild, dmHelpKey))
                {
                    ViverLog.Warning("DmHelp subscription at startup failed! GuildId: {GuildId}", guild.Id);
                }
            }
        }

        /// <summary>
        ///     Locks all currently active threads.
        /// </summary>
        /// <returns></returns>
        private async Task LockActiveFanficThreads()
        {
            var dbContext = _serviceProvider.GetRequiredService<ViverContext>();

            var fanfics = await dbContext.Fanfics.Where(f => f.IsActive).ToArrayAsync();
            foreach (var fanfic in fanfics)
            {
                var thread = (DiscordThreadChannel)await Client.GetChannelAsync(fanfic.ThreadId);

                var embed = new DiscordEmbedBuilder()
                    .WithTitle("This thread is currently locked")
                    .WithDescription(
                        $"This thread is locked due to a short maintenance of the bot.\n" +
                        $"If this takes longer than usual, please contact staff.\n\n" +
                        $"<t:{DateTimeOffset.UtcNow.ToUnixTimeSeconds()}:R>");
                var message = await thread.SendMessageAsync(embed);
                await thread.ModifyAsync(t => t.Locked = true);
                fanfic.LockMessageId = message.Id;
            }

            await dbContext.SaveChangesAsync();
        }

        /// <summary>
        ///     Gets called when the bot starts.
        /// </summary>
        /// <param name="token">
        ///     Notifies when operations should be canceled.
        /// </param>
        /// <returns></returns>
        public async Task StartAsync(CancellationToken token)
        {
            string name;
            if (_configuration.GetValue<bool>("fanficNumberInStatus"))
            {
                var fanficObserver = _serviceProvider.GetRequiredService<FanficObserver>();
                var fanficCount = fanficObserver.GetCountOfObservers();

                name = fanficCount + " fanfics";
            }
            else
            {
                name = "you awesome people";
            }

            var activity = new DiscordActivity()
            {
                ActivityType = ActivityType.ListeningTo,
                Name = name
            };

            await Client.ConnectAsync(activity, UserStatus.Online);
            ViverLog.ClientConnected(Client);
        }

        /// <summary>
        ///     Gets called when the bot stops.
        /// </summary>
        /// <param name="token">
        ///     Notifies when operations should be canceled.
        /// </param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken token)
        {
            await LockActiveFanficThreads();

            await Client.DisconnectAsync();
            await Log.CloseAndFlushAsync();
            Client.Dispose();
        }
    }
}
