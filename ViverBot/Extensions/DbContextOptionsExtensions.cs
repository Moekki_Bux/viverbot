﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;

namespace ViverBot.Extensions
{
    public static class DbContextOptionsExtensions
    {
        /// <summary>
        ///     Configures the context to a MySql compatible database.
        /// </summary>
        /// <param name="optionsBuilder">
        ///     The builder being used to configure the context.
        /// </param>
        /// <param name="connectionString">
        ///     The connection string of the database to connect to.
        /// </param>
        /// <returns>
        ///     The options builder so that further configuration can be chained.
        /// </returns>
        public static DbContextOptionsBuilder UseMySql(this DbContextOptionsBuilder optionsBuilder, string connectionString)
        {
            return optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
        }
    }
}
