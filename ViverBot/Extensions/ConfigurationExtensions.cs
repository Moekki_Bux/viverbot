﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ViverBot.Extensions
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        ///     Loads settings from the default settings file for ViverBot.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns>
        ///     The same instance of the <see cref="IConfigurationBuilder"/> for chaining.
        /// </returns>
        public static IConfigurationBuilder LoadConfiguration(this IConfigurationBuilder configuration)
        {
            string appPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string path = Path.Combine(appPath, "viverbot", "settings");
            string filePath = ViverSettings.GetSettingsFile(path);

            return configuration.AddJsonFile(filePath, false);
        }
    }
}
