﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using System.Text.RegularExpressions;

namespace ViverBot.Extensions
{
    public static partial class StringExtensions
    {
        /// <summary>
        ///     This takes a max length and searches before that for the last ocurrence
        ///     of <paramref name="splitChar"/>. Then it splits the string on that.
        /// </summary>
        /// <param name="str">
        ///     The original <see cref="string"/>.
        /// </param>
        /// <param name="maxLength">
        ///     The maximal length of the substrings.
        /// </param>
        /// <param name="splitChar">
        ///     The character to split the string on.
        /// </param>
        /// <returns>
        ///     A <see cref="IEnumerable{T}"/> of strings.
        /// </returns>
        public static IEnumerable<string> SplitByLength(this string str, int maxLength, char splitChar)
        {
            int index = 0;
            while (index + maxLength < str.Length)
            {
                var tempStr = str.Substring(index, maxLength);
                if (tempStr.StartsWith(splitChar))
                {
                    index++;
                    tempStr = str.Substring(index, maxLength);
                }

                int splitCharIndex = tempStr.LastIndexOf(splitChar);

                yield return str.Substring(index, splitCharIndex + 1);
                index += splitCharIndex;
            }

            yield return str[index..];
        }

        /// <summary>
        ///     Turns a string representation of a log level into the actual LogLevel.
        /// </summary>
        /// <param name="str">
        ///     The string that contains the log level.
        /// </param>
        /// <returns>
        ///     A <see cref="LogLevel"/>.
        /// </returns>
        public static LogLevel ToLogLevel(this string str)
            => str.ToLower() switch
            {
                "trace" => LogLevel.Trace,
                "debug" => LogLevel.Debug,
                "information" => LogLevel.Information,
                "warning" => LogLevel.Warning,
                "error" => LogLevel.Error,
                "critical" => LogLevel.Critical,
                "none" => LogLevel.None,
                _ => LogLevel.Information
                //_ => throw new ArgumentException("the requested log level doesn't exist!")
            };

        public static string ToKeyRepresentation(this string key)
        {
            return NonLettersRegex().Replace(key.Trim().ToLower(), string.Empty);
        }

        [GeneratedRegex("[^a-z\\s]")]
        private static partial Regex NonLettersRegex();

        /// <summary>
        ///     Compares 2 strings and returns the Levenshtein distance.
        /// </summary>
        /// <remarks>
        ///     This function is shamelessly stolen from https://github.com/DanHarltey/Fastenshtein
        /// </remarks>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>
        ///     The difference between the strings. 0 means they are equal.
        /// </returns>
        public static int LevenshteinDistance(this string value1, string value2)
        {
            if (value2.Length == 0)
            {
                return value1.Length;
            }

            int[] costs = new int[value2.Length];

            // Add indexing for insertion to first row
            for (int i = 0; i < costs.Length;)
            {
                costs[i] = ++i;
            }

            for (int i = 0; i < value1.Length; i++)
            {
                // cost of the first index
                int cost = i;
                int previousCost = i;

                // cache value for inner loop to avoid index lookup and bonds checking, profiled this is quicker
                char value1Char = value1[i];

                for (int j = 0; j < value2.Length; j++)
                {
                    int currentCost = cost;

                    // assigning this here reduces the array reads we do, improvement of the old version
                    cost = costs[j];

                    if (value1Char != value2[j])
                    {
                        if (previousCost < currentCost)
                        {
                            currentCost = previousCost;
                        }

                        if (cost < currentCost)
                        {
                            currentCost = cost;
                        }

                        ++currentCost;
                    }

                    /* 
                     * Improvement on the older versions.
                     * Swapping the variables here results in a performance improvement for modern intel CPU’s, but I have no idea why?
                     */
                    costs[j] = currentCost;
                    previousCost = currentCost;
                }
            }

            return costs[^1];
        }

        [GeneratedRegex("(?<=<:\\w+:)\\d+(?=>)")]
        private static partial Regex DiscordEmojiRegex();

        public static DiscordEmoji? GetEmoji(this string str, BaseDiscordClient client)
        {
            if (DiscordEmoji.TryFromUnicode(client, str, out DiscordEmoji? emoji) ||
                DiscordEmoji.TryFromName(client, str, out emoji))
            {
            }
            else if (
                ulong.TryParse(str, out ulong id) &&
                DiscordEmoji.TryFromGuildEmote(client, id, out emoji))
            {
            }
            else
            {
                var match = DiscordEmojiRegex().Match(str);
                if (match.Success &&
                    DiscordEmoji.TryFromGuildEmote(client, ulong.Parse(match.Value), out emoji))
                {
                }
            }

            return emoji;
        }
    }
}
