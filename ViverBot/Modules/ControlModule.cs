﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using ViverBot.Logging;
using ViverBot.Services;
using ViverBot.Services.FanficServices;

namespace ViverBot.Modules
{
    [SlashRequireUserPermissions(Permissions.ManageGuild)]
    [SlashCommandGroup("control", "commands for managing the bot")]
    public class ControlModule : ApplicationCommandModule
    {
        private readonly IResponseService _responseService;
        private readonly IFanficService _fanficService;

        public ControlModule(IResponseService responseService, IFanficService fanficService)
        {
            _responseService = responseService;
            _fanficService = fanficService;
        }

        [SlashCommand("activity", "edit the activity of the bot")]
        public async Task UpdateActivity(InteractionContext context,
            [Option("activitytype", "the type of activity")] ActivityType activityType,
            [MaximumLength(128)]
            [Option("name", "the name of the activity")] string name,
            [Option("status", "the status of the bot. OPTIONAL")] UserStatus userStatus = UserStatus.Online)
        {
            var activity = new DiscordActivity()
            {
                ActivityType = activityType,
                Name = name
            };

            var embed = _responseService.ActivityUpdatedEmbed(activity, userStatus, context.Member);
            await context.CreateResponseAsync(embed);

            await context.Client.UpdateStatusAsync(activity, userStatus);
            ViverLog.ActivityUpdated(context.Client, context.Member);
        }

        [SlashCommand("status", "edit the status of the bot")]
        public async Task UpdateStatus(InteractionContext context,
            [Option("status", "the status of the bot")] UserStatus userStatus)
        {
            var embed = _responseService.StatusUpdatedEmbed(userStatus, context.Member);
            await context.CreateResponseAsync(embed);

            await context.Client.UpdateStatusAsync(null, userStatus);
            ViverLog.StatusUpdated(context.Client, context.Member);
        }

        [SlashCommand("reconnect", "tries to reconnect the client")]
        public async Task Reconnect(InteractionContext context)
        {
            var embed = _responseService.ReconnectEmbed(context.Member);
            await context.CreateResponseAsync(embed);

            await context.Client.ReconnectAsync(true);
            ViverLog.ReconnectRequested(context.Member);
        }

        [SlashCommand("check", "checks for any currently active fanfics")]
        public async Task AnyActiveFanfics(InteractionContext context)
        {
            var anyFound = await _fanficService.AnyActiveFanficsAsync();

            var response = anyFound
                ? "There are active fanfics"
                : "There are **no** active fanfics";
            var embed = new DiscordEmbedBuilder()
                .WithDescription(response);
            await context.CreateResponseAsync(embed);
        }
    }
}
