﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using ViverBot.Enums;
using ViverBot.Logging;
using ViverBot.Services;

namespace ViverBot.Modules
{
    /// <summary>
    ///     SlashCommand module with core commands.
    /// </summary>
    public class CoreModule : ApplicationCommandModule
    {
        private readonly IResponseService _responseService;

        public CoreModule(IResponseService responseService)
        {
            _responseService = responseService;
        }

        /// <summary>
        ///     Returns th ping.
        /// </summary>
        /// <param name="context">
        ///     The <see cref="InteractionContext"/>
        /// </param>
        /// <returns></returns>
        [SlashCommand("ping", "returns the ping")]
        public async Task Ping(InteractionContext context)
        {
            var content = _responseService.PingContent(context.Client);
            await context.CreateResponseAsync(content);
        }

        /// <summary>
        ///     Prints information about the bot.
        /// </summary>
        /// <param name="context">
        ///     <inheritdoc cref="Ping(InteractionContext)" path="/param[@name='context']"/>
        /// </param>
        /// <returns></returns>
        [SlashCommand("about", "gives information about the bot")]
        public async Task About(InteractionContext context)
        {
            var embed = _responseService.AboutEmbed(context.Client);
            await context.CreateResponseAsync(embed);
        }

        /// <summary>
        ///     Counts all users in the server excluding bots.
        /// </summary>
        /// <param name="context">
        ///     <inheritdoc cref="Ping(InteractionContext)" path="/param[@name='context']"/>
        /// </param>
        /// <param name="role">
        ///     If set, counts only users with this <see cref="DiscordRole"/>.
        /// </param>
        /// <returns></returns>
        [SlashCommand("usercount", "returns the count of users on the server excluding bots")]
        public async Task UserCount(InteractionContext context,
            [Option("role", "counts all users with the specified role. OPTIONAL")] DiscordRole? role = null)
        {

            IEnumerable<DiscordMember> allMembers = await context.Guild.GetAllMembersAsync();
            if (role is not null)
            {
                allMembers = allMembers.Where(m => m.Roles.Contains(role));
            }

            var userCount = allMembers.Where(m => m.IsBot == false).Count();
            var botCount = allMembers.Where(m => m.IsBot).Count();

            var embed = _responseService.UserCountEmbedAsync(userCount, botCount, role);
            await context.CreateResponseAsync(embed);
        }

        /// <summary>
        ///     Gets the full size image of a sticker.
        /// </summary>
        /// <param name="context">
        ///     <inheritdoc cref="Ping(InteractionContext)" path="/param[@name='context']"/>
        /// </param>
        /// <param name="url">
        ///     The url of a message containing the sticker.
        /// </param>
        /// <returns></returns>
        [SlashCommand("getsticker", "get the full size image of a sticker")]
        public async Task GetSticker(InteractionContext context,
            [Option("messageUrl", "the url of the message")] string url)
        {
            var rawId = url.Split("/").LastOrDefault();
            if (ulong.TryParse(rawId, out ulong id))
            {
                var message = await context.Channel.GetMessageAsync(id);
                if (message.Stickers.Count > 0)
                {
                    await context.CreateResponseAsync(message.Stickers[0].StickerUrl);
                }
                else
                {
                    await context.CreateResponseAsync("The linked message does not contain any stickers.", true);
                    ViverLog.Warning("The linked message does not contain any stickers. {Url}", url);
                }

                return;
            }

            await context.CreateResponseAsync("The provided url does not direct to a Discord message.", true);
            ViverLog.Warning("The provided url does not direct to a Discord message. {Url}", url);
        }

        /// <summary>
        ///     Brews coffee according to RFC2324.
        /// </summary>
        /// <param name="context">
        ///     <inheritdoc cref="Ping(InteractionContext)" path="/param[@name='context']"/>
        /// </param>
        /// <param name="milkType">
        ///     The type of milk added to the coffee.
        /// </param>
        /// <param name="syrupType">
        ///     The type of syrup added to the coffee.
        /// </param>
        /// <param name="alcoholType">
        ///     The type of alcohol added to the coffee.
        /// </param>
        /// <returns></returns>
        [SlashCommand("coffee", "brews coffee")]
        public async Task BrewCoffee(InteractionContext context,
#pragma warning disable IDE0060
            [Option("milk-type", "the type of milk to be used")] MilkType milkType = MilkType.None,
            [Option("syrup-type", "the type of syrup to be used")] SyrupType syrupType = SyrupType.None,
            [Option("alcohol-type", "the type of alcohol to be used")] AlcoholType alcoholType = AlcoholType.None)
#pragma warning restore
        {
            var embed = _responseService.CoffeeEmbed();
            await context.CreateResponseAsync(embed);
        }

        /// <summary>
        ///     Pop the bubbles! Why? Because it's fun!
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [SlashCooldown(1, 10, SlashCooldownBucketType.User)]
        [SlashCooldown(1, 10, SlashCooldownBucketType.Channel)]
        [SlashCommand("bubblewrap", "Pop the bubbles! Why? Because it's fun!")]
        public async Task Bubblewrap(InteractionContext context)
        {
            string bubbles =
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||\r\n" +
                "||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop|| ||pop||";

            await context.CreateResponseAsync(bubbles);
        }
    }
}
