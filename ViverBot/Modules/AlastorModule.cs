﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using ViverBot.Services;

namespace ViverBot.Modules
{
    [SlashCommandGroup("alastor",
        "Feed Alastor every day to increase your streak! When Alastor gets upset, offer him a crown!")]
    public class AlastorModule : ApplicationCommandModule
    {
        private readonly IAlastorService _alastorService;
        private readonly IResponseService _responseService;

        private const string gif0 = "https://cdn.discordapp.com/attachments/1125503011528134706/1125503498113523712/alastor-0.gif";
        private const string gif1 = "https://cdn.discordapp.com/attachments/1125503011528134706/1125503517369581609/alastor-1.gif";
        private const string gif2 = "https://cdn.discordapp.com/attachments/1125503011528134706/1125503544758378597/alastor-2.gif";
        private const string gif3 = "https://cdn.discordapp.com/attachments/1125503011528134706/1125503563196534845/alastor-3.gif";

        public AlastorModule(IAlastorService alastorService, IResponseService responseService)
        {
            _alastorService = alastorService;
            _responseService = responseService;
        }

        [SlashCommand("feed", "Feed Alastor everyday to increase your streak!")]
        public async Task Feed(InteractionContext context)
        {
            DiscordEmbed embed;
            var alastor = await _alastorService.GetAlastorAsync(context.Member.Id);
            if (alastor is null)
            {
                alastor = await _alastorService.AddAlastorAsync(context.Member.Id);

                embed = _responseService.AlastorEmbed(
                    context.Member,
                    alastor,
                    context.Member.DisplayName + " fed Alastor!",
                    gif0);
            }
            else
            {
                if (alastor.LastCrown.AddDays(7) < DateTimeOffset.UtcNow)
                {
                    embed = _responseService.AlastorEmbed(
                        context.Member,
                        alastor,
                        "Alastor is ready to battle. Try calming him down with a gift, quickly! Try the crown!",
                        gif2);
                }
                else
                {
                    if (alastor.LastFed < DateTimeOffset.UtcNow)
                    {
                        if (alastor.LastFed.AddDays(1) < DateTimeOffset.UtcNow)
                        {
                            alastor.CurrentStreak++;
                            alastor.HighestStreak = Math.Max(alastor.CurrentStreak, alastor.HighestStreak);

                            embed = _responseService.AlastorEmbed(
                                context.Member,
                                alastor,
                                context.Member.DisplayName + " fed Alastor!",
                                gif0);
                        }
                        else
                        {
                            alastor.CurrentStreak = 1;

                            embed = _responseService.AlastorEmbed(
                                context.Member,
                                alastor,
                                context.Member.DisplayName + ", Alastor has stopped broadcasting",
                                gif0);
                        }

                        alastor.LastFed = DateTimeOffset.UtcNow;
                        await _alastorService.UpdateAlastorAsync(alastor);

                    }
                    else
                    {
                        embed = _responseService.AlastorEmbed(
                            context.Member,
                            alastor,
                            context.Member.DisplayName + "'s Alastor",
                            gif1);
                    }
                }
            }

            await context.CreateResponseAsync(embed);
        }

        [SlashCommand("crown", "When Alastor gets upset, offer him a crown to appease him.")]
        public async Task Crown(InteractionContext context)
        {
            DiscordEmbed embed;
            var alastor = await _alastorService.GetAlastorAsync(context.Member.Id);
            alastor ??= await _alastorService.AddAlastorAsync(context.Member.Id);

            if (alastor.LastCrown.AddDays(7) < DateTimeOffset.UtcNow)
            {
                embed = _responseService.AlastorEmbed(
                    context.Member,
                    alastor,
                    context.Member.DisplayName + ", Alastor doesn't need a crown",
                    gif0);
            }
            else if (alastor.Crowns > 0)
            {
                alastor.Crowns--;

                embed = _responseService.AlastorEmbed(
                    context.Member,
                    alastor,
                    "That seems to have done the trick, he seems content. He will allow you to claim the daily streak... maybe.",
                    gif3);
            }
            else
            {
                embed = _responseService.AlastorEmbed(
                    context.Member,
                    alastor,
                    context.Member.DisplayName + ", you do not have any crowns!",
                    gif2);
            }

            await context.CreateResponseAsync(embed);
        }

        [SlashCommand("show", "Show everyone what a cute Alastor you have!")]
        public async Task Display(InteractionContext context)
        {
            var alastor = await _alastorService.GetAlastorAsync(context.Member.Id);
            alastor ??= await _alastorService.AddAlastorAsync(context.Member.Id);

            var rnd = new Random();
            var gif = rnd.Next(1, 3) == 1 ? gif0 : gif1;

            var embed = _responseService.AlastorEmbed(
                context.Member,
                alastor,
                context.Member.DisplayName + "'s Alastor",
                gif);

            await context.CreateResponseAsync(embed);
        }
    }
}
