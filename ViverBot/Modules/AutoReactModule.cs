﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using System.Text;
using ViverBot.Logging;
using ViverBot.Services.AutoReactServices;

namespace ViverBot.Modules
{
    [GuildOnly]
    [SlashCommandGroup("autoreact", "set of commands for autoreact channels")]
    public class AutoReactModule : ApplicationCommandModule
    {
        private readonly AutoReactObserver _autoReactObserver;
        private readonly IAutoReactService _autoReactService;

        public AutoReactModule(AutoReactObserver autoReactObserver,
            IAutoReactService autoReactService)
        {
            _autoReactObserver = autoReactObserver;
            _autoReactService = autoReactService;
        }

        [GuildOnly]
        [SlashRequireUserPermissions(Permissions.ManageChannels)]
        [SlashCommand("add", "Adds auto reactions from a channel")]
        public async Task AddAutoReactChannel(InteractionContext context,
            [Option("channel", "the channel to listen to")] DiscordChannel channel,
            [Option("choices", "a comma separated list of emojis for reactions")] string emojisString)
        {
            var reactions = emojisString.Replace(" ", "").Split(',').ToList();
            if (_autoReactObserver.Subscribe(context.Client, channel, reactions))
            {
                await _autoReactService.AddAutoReactAsync(context.Guild.Id, channel.Id, string.Join(',', reactions));

                var embed = new DiscordEmbedBuilder()
                    .WithDescription("AutoReaction was set to channel " + channel.Mention);
                await context.CreateResponseAsync(embed);
            }
            else
            {
                ViverLog.Error("Something went wrong while subscribing a autoReact!");
                await context.CreateResponseAsync(
                    "The autoReact couldn't be subscribed! " +
                    "Maybe there is already a autoReact on this channel? " +
                    "If you believe this is an error, contact the developer.");
                return;
            }
        }

        [GuildOnly]
        [SlashRequireUserPermissions(Permissions.ManageChannels)]
        [SlashCommand("remove", "removes an auto reaction from a channel")]
        public async Task RemoveAutoReactChannel(InteractionContext context,
            [Option("channel", "the channel to remove from auto reactions")] DiscordChannel channel)
        {
            if (_autoReactObserver.Unsubscribe(context.Client, channel.Id))
            {
                await _autoReactService.RemoveAutoReactAsync(context.Guild.Id, channel.Id);

                var embed = new DiscordEmbedBuilder()
                    .WithDescription("AutoReaction was removed from channel " + channel.Mention);
                await context.CreateResponseAsync(embed);
            }
            else
            {
                ViverLog.Error("Something went wrong while unsubscribing a autoReact!");
                await context.CreateResponseAsync(
                    "The autoReact couldn't be unsubscribed! " +
                    "Maybe there is no autoReact on this channel? " +
                    "If you believe this is an error, contact the developer.");
                return;
            }
        }

        [GuildOnly]
        [SlashCommand("show", "Show all channels where the autoReact is listening to.")]
        public async Task ShowAutoReactChannels(InteractionContext context)
        {
            var sb = new StringBuilder();
            var autoReacts = await _autoReactService.GetAutoReactsAsync(context.Guild.Id);
            foreach (var autoReact in autoReacts)
            {
                try
                {
                    var channel = await context.Client.GetChannelAsync(autoReact.ChannelId);
                    sb.AppendLine(channel.Mention);
                }
                catch
                {
                    ViverLog.Warning($"Channel with id {autoReact.ChannelId} was not found!");
                }
            }

            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Blurple)
                .WithTitle("Active autoReact channels")
                .WithDescription(sb.ToString());

            await context.CreateResponseAsync(embed, true);
        }
    }
}
