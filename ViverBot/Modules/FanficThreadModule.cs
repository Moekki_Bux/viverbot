﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using Microsoft.Extensions.Options;
using Serilog;
using ViverBot.Attributes;
using ViverBot.Logging;
using ViverBot.Services;
using ViverBot.Services.FanficServices;

namespace ViverBot.Modules
{
    [GuildOnly]
    [SlashRequireBotPermissions(Permissions.CreatePublicThreads |
                                Permissions.SendMessagesInThreads |
                                Permissions.ManageThreads |
                                Permissions.ManageMessages |
                                Permissions.ReadMessageHistory)]
    [SlashCommandGroup("fanfic", "set of commands related to community made fanfics")]
    public class FanficThreadModule : ApplicationCommandModule
    {
        private readonly FanficObserver _fanficObserver;
        private readonly IResponseService _responseService;
        private readonly IFanficService _fanficService;
        private readonly bool showFanficNumberInStatus;

        public FanficThreadModule(FanficObserver fanficObserver,
                                  IResponseService responseService,
                                  IFanficService fanficService,
                                  IOptions<ViverSettings> settings)
        {
            _fanficObserver = fanficObserver;
            _responseService = responseService;
            _fanficService = fanficService;
            showFanficNumberInStatus = settings.Value.FanficNumberInStatus;
        }

        [SlashCommand("start", "starts a new fanfic")]
        public async Task StartFanficThread(InteractionContext context,
            [Option("name", "the name of the fanfic")] string name)
        {
            var embed = _responseService.StartFanficThreadEmbed(context.Member, name);
            await context.CreateResponseAsync(embed);

            var message = await context.GetOriginalResponseAsync();
            var thread = await context.Channel.CreateThreadAsync(
                message,
                name,
                AutoArchiveDuration.ThreeDays,
                $"New fanfic created: \"{name}\"");
            await thread.JoinThreadAsync();

            if (_fanficObserver.Subscribe(context.Client, thread))
            {
                await _fanficService.AddFanficAsync(
                    name,
                    context.Member.Id,
                    thread.Id,
                    context.Channel.Id,
                    context.Guild.Id);

                if (showFanficNumberInStatus)
                {
                    var count = _fanficObserver.GetCountOfObservers();
                    var activity = new DiscordActivity($"{count} fanfics", ActivityType.ListeningTo);
                    await context.Client.UpdateStatusAsync(activity);
                }
            }
            else
            {
                var owner = context.Client.CurrentApplication.Owners.First().Mention;
                await thread.SendMessageAsync($"{owner} **Subscribe failed when starting this fanfic!**");
                await thread.ModifyAsync(t => t.Locked = true);
            }
        }

        [SlashRequireThread]
        [SlashCommand("stop", "stops the current fanfic")]
        public async Task StopFanficThread(InteractionContext context)
        {
            var thread = (DiscordThreadChannel)context.Channel;
            var fanfic = await _fanficService.GetFanficAsync(context.Guild.Id, thread.Id);
            if (fanfic is null)
            {
                ViverLog.CommandFailed(context, "NoFanfic");
                var noFanficEmbed = _responseService.NoFanficEmbed();
                await context.CreateResponseAsync(noFanficEmbed, true);
                return;
            }

            // Only the initial author of the fanfic or a moderator should be able to end a fanfic
            if (context.Member.Id == fanfic.AuthorId ||
                context.Member.Permissions.HasFlag(Permissions.ManageMessages | Permissions.Administrator))
            {
                if (_fanficObserver.Unsubscribe(context.Client, thread.Id))
                {
                    await _fanficService.DeactivateFanficAsync(fanfic);

                    var embed = _responseService.EndFanficEmbed(context.Member);
                    await context.CreateResponseAsync(embed);

                    await thread.ModifyAsync(t => t.Locked = true);

                    if (showFanficNumberInStatus)
                    {
                        var count = _fanficObserver.GetCountOfObservers();
                        var activity = new DiscordActivity($"{count} fanfics", ActivityType.ListeningTo);
                        await context.Client.UpdateStatusAsync(activity);
                    }
                }
                else
                {
                    var msg = await thread.SendMessageAsync("**Unsubscribe failed when ending this fanfic!**");
                    await thread.ModifyAsync(t => t.Locked = true);
                    Log.Error("Unsubscribe failed when ending a fanfic!");
                    var owner = (DiscordMember)context.Client.CurrentApplication.Owners.First();
                    await owner.SendMessageAsync($"**Unsubscribe failed when ending a fanfic!** {msg.JumpLink}");
                }
            }
            else
            {
                ViverLog.CommandFailed(context, "UnauthorizedFanfic");
                var unauthorizedEmbed = _responseService.UnauthorizedFanficEmbed();
                await context.CreateResponseAsync(unauthorizedEmbed, true);
            }
        }
    }
}
