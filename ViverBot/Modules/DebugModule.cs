﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus.Entities;
using DSharpPlus;
using DSharpPlus.SlashCommands;
using ViverBot.Enums;
using ViverBot.Extensions;

namespace ViverBot.Modules
{
    public class DebugModule : ApplicationCommandModule
    {

        [SlashCommand("test", "test")]
        public async Task Test(InteractionContext context)
        {
            await context.DeferAsync();

            string r = "";
            foreach (var channel in context.Guild.Channels.Values.Where(c => c.Type == ChannelType.Text))
            {
                var threads = await channel.ListPublicArchivedThreadsAsync();
                foreach (var thread in threads.Threads)
                {
                    r += thread.Name + "\n";
                }
            }

            await context.EditResponseAsync(new DiscordWebhookBuilder().WithContent(r));
        }

        [SlashCommand("permission", "sets the required permission to end a fanfic")]
        public async Task SetEndPermission(InteractionContext context,
            [Option("permission", "the required permission")] PermissionOption permission)
        {
            Enum.TryParse(typeof(Permissions), permission.ToString(), out object? perm);
            if (perm is not null)
            {
                var newPerm = (Permissions)perm;
                await context.CreateResponseAsync(newPerm.ToString() + " : " + (long)newPerm);
            }

            await context.CreateResponseAsync(permission.ToString());
        }

        [SlashCommand("role", "sets the required role to end a fanfic")]
        public async Task SetEndRole(InteractionContext context,
            [Option("role", "the required role")] DiscordRole role)
        {
            await context.CreateResponseAsync(role.Mention);
        }

        [SlashCommand("countMessages", "counts messages")]
        public async Task CountMessages(InteractionContext context,
            [Option("channel", "the channel")] DiscordChannel channel,
            [Option("messageId", "the message")] string messageId)
        {
            await context.DeferAsync();

            ulong parsedMessageId = ulong.Parse(messageId);
            var lastMessage = (await channel.GetMessagesAsync(15)).Where(m => !m.Author.IsBot).First();
            var messages = new List<DiscordMessage>();
            bool allMessagesFetched = false;
            do
            {
                var messagesAfter = await channel.GetMessagesAfterAsync(parsedMessageId);
                messages.AddRange(messagesAfter.Reverse());
                if (messages[^1].Id == lastMessage.Id)
                {
                    allMessagesFetched = true;
                }
                else
                {
                    parsedMessageId = messages[^1].Id;
                }
            } while (!allMessagesFetched);

            await context.EditResponseAsync(new DiscordWebhookBuilder().WithContent(messages.Count.ToString()));
        }

        [SlashCommand("getmachine", "get the name of the running machine")]
        public async Task GetMachineName(InteractionContext context)
        {
            await context.CreateResponseAsync(Environment.MachineName);
        }

        [SlashCommand("printemoji", "prints an emoji")]
        public async Task GetEmoji(InteractionContext context,
            [Option("emoji", "A emoji as text")] string emojiStr)
        {
            var emoji = emojiStr.GetEmoji(context.Client);

            await context.CreateResponseAsync(emoji);
        }
    }
}
