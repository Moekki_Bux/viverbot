﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using ViverBot.Logging;
using ViverBot.Services.DmHelpServices;

namespace ViverBot.Modules
{
    [SlashRequireUserPermissions(Permissions.ManageMessages)]
    [SlashCommandGroup("dmhelp", "for dm help messages")]
    public class DmHelpModule : ApplicationCommandModule
    {
        private readonly DmHelpObserver _dmHelpObserver;
        private readonly IDmHelpService _dmHelpService;

        public DmHelpModule(DmHelpObserver dmHelpObserver, IDmHelpService dmHelpService)
        {
            _dmHelpObserver = dmHelpObserver;
            _dmHelpService = dmHelpService;
        }

        [SlashRequireUserPermissions(Permissions.ManageMessages)]
        [SlashCommand("add", "When the specified key is mentioned, the user is dmed with the specified response")]
        public async Task AddDmHelp(InteractionContext context,
            [Option("key", "A word or phrase to which the bot should respond")] string key,
            [Option("response", "The message with which the bot should respond. \\n for new line")] string responseContent)
        {
            await context.DeferAsync();

            responseContent = responseContent.Replace("\\n", "\n");
            await _dmHelpService.AddDmHelpAsync(context.Guild.Id, key, responseContent);
            var dmHelpKey = await _dmHelpService.GetDmHelpKeyAsync(context.Guild.Id, key);
            if (dmHelpKey is not null && dmHelpKey.DmHelpResponse is not null)
            {
                if (_dmHelpObserver.Subscribe(context.Client, context.Guild, dmHelpKey))
                {
                    ViverLog.Information("{Member} added a help trigger with key {Key}", context.Member.Username, dmHelpKey.Value);
                    var embed = new DiscordEmbedBuilder()
                        .WithAuthor(context.Member.Username, null, context.Member.AvatarUrl)
                        .WithTitle("A new help trigger was added")
                        .AddField("Key", dmHelpKey.Value)
                        .AddField("Response", dmHelpKey.DmHelpResponse.Text)
                        .WithFooter($"UID: {context.Member.Id} | KID: {dmHelpKey.Id} | RID: {dmHelpKey.DmHelpResponse.Id}");
                    await context.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
                    return;
                }

                ViverLog.Warning("DmHelp subscription failed!");
                await context.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The help trigger couldn't be created!"));
                return;
            }

            ViverLog.Warning("DmHelp: Key or Response is null!");
            await context.EditResponseAsync(new DiscordWebhookBuilder().WithContent("This help trigger doesn't exist!"));
        }

        [SlashRequireUserPermissions(Permissions.ManageMessages)]
        [SlashCommand("remove", "Remove the specified key and it's response")]
        public async Task RemoveDmHelp(InteractionContext context,
            [Option("key", "A word or phrase to which the bot responds")] string key)
        {
            await context.DeferAsync();

            var dmHelpKey = await _dmHelpService.GetDmHelpKeyAsync(context.Guild.Id, key);
            if (dmHelpKey is not null && _dmHelpObserver.Unsubscribe(context.Client, dmHelpKey))
            {
                var embed = new DiscordEmbedBuilder()
                    .WithAuthor(context.Member.Username, null, context.Member.AvatarUrl)
                    .WithTitle("A help trigger was deleted")
                    .AddField("Key", dmHelpKey.Value)
                    .AddField("Response", dmHelpKey.DmHelpResponse!.Text)
                    .WithFooter($"UID: {context.Member.Id} | KID: {dmHelpKey.Id} | RID: {dmHelpKey.DmHelpResponse.Id}")
                    .Build();
                if (await _dmHelpService.DeleteDmHelpAsync(dmHelpKey))
                {
                    ViverLog.Information("{Member} deleted a help trigger with key {Key}", context.Member.Username, dmHelpKey.Value);
                    await context.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
                    return;
                }

                ViverLog.Warning("DmHelp: Key or Response couldn't be deleted! Key: {Key}", dmHelpKey.Value);
                await context.EditResponseAsync(
                    new DiscordWebhookBuilder().WithContent("https://xkcd.com/2200"));
                return;
            }

            ViverLog.Warning("DmHelp unsubscribe failed!");
            await context.EditResponseAsync(new DiscordWebhookBuilder().WithContent("The help trigger couldn't be removed!"));
        }

        [SlashCommand("list", "lists all dm help keys")]
        public async Task ListDmHelps(InteractionContext context)
        {
            await context.DeferAsync();

            var dmHelpKeys = await _dmHelpService.GetDmHelpKeysAsync(context.Guild.Id);
            string content = string.Join("\n", dmHelpKeys.Select(k => k.Value));

            var embed = new DiscordEmbedBuilder()
                .WithTitle("Active dmhelp messages")
                .WithDescription(content);

            await context.EditResponseAsync(new DiscordWebhookBuilder().AddEmbed(embed));
        }

        [SlashCommand("view", "shows the response message for a key")]
        public async Task ViewDmHelp(InteractionContext context,
            [Option("key", "The key of a dmHelp response")] string key)
        {
            var dmHelpKey = await _dmHelpService.GetDmHelpKeyAsync(context.Guild.Id, key);
            if (dmHelpKey is not null)
            {
                var embed = new DiscordEmbedBuilder()
                    .WithTitle(dmHelpKey.Value)
                    .WithDescription(dmHelpKey.DmHelpResponse!.Text);
                await context.CreateResponseAsync(embed);
                return;
            }

            await context.CreateResponseAsync($"A dmHelp with key `{key}` doesn't exist!");
        }
    }
}
