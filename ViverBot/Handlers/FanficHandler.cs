﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using System.Text.RegularExpressions;
using ViverBot.Logging;

namespace ViverBot.Handlers
{
    /// <summary>
    ///     Handler for fanfic related events.
    /// </summary>
    public static partial class FanficHandler
    {
        /// <summary>
        ///     Verifies and manages messages sent to a fanfic.
        /// </summary>
        /// <param name="sender">
        ///     The <see cref="DiscordClient"/> of this event.
        /// </param>
        /// <param name="e">
        ///     The <see cref="MessageCreateEventArgs"/> of this event.
        /// </param>
        /// <param name="channel">
        ///     The <see cref="DiscordChannel"/> in which the fanfic is running.
        /// </param>
        /// <returns></returns>
        public static async Task VerifyFanficMessage(
            DiscordClient sender,
            MessageCreateEventArgs e,
            DiscordChannel channel)
        {
            // The handler should only be active in the fanfic channel
            if (e.Channel.Id != channel.Id)
            {
                return;
            }

            // Don't verify bot messages
            if (e.Author.IsBot)
            {
                return;
            }

            // We also don't want to check interactions and bots
            var lastMessages = (await channel.GetMessagesAsync(10))
                .Where(m => !m.Author.IsBot)
                .TakeWhile(m => m.Interaction is null);
            var msg = lastMessages.First(m => !m.Author.IsBot);
            // Users aren't allowed to send multiple messages in a row
            // To change the delay
            // to wait before a user      ||
            // can send another message,  ||
            // change this value          \/
            if (lastMessages.Skip(1).Take(1).Where(m => m.Author == e.Author).Any())
            {
                await DeleteMessageAsync(
                    msg,
                    "You were the last one who posted a message.");
                return;
            }

            var content = msg.Content.ToUpper();
            // Remove all ocurrences of exclusions from the string for easier verification
            for (int i = 0; i < Exclusions.Length; i++)
            {
                content = Regex.Replace(content, "\\b" + Regex.Escape(Exclusions[i].ToUpper()), "");
            }

            // test for quotes
            switch (content.Count(c => c == '"'))
            {
                // verify the subsentence
                case 2:
                    var firstPos = content.IndexOf('"');
                    var secondPos = content.IndexOf('"', firstPos + 1);
                    var subString = content.Substring(firstPos + 1, secondPos - firstPos - 1);

                    if (OneSentenceRegex().IsMatch(subString))
                    {
                        await DeleteMessageAsync(
                            msg,
                            "Your message contained more than one sentence.");
                        return;
                    }

                    content = content.Remove(firstPos, secondPos - firstPos + 1);
                    break;
                // more than 2 quotemarks aren't allowed
                case > 2:
                    await DeleteMessageAsync(
                        msg,
                        "Due to technical restrictions, it is not possible to have more than 2 quotation marks in a sentence.");
                    return;
            }

            // Verify that there is only one sentence in the message
            if (OneSentenceRegex().IsMatch(content))
            {
                await DeleteMessageAsync(
                    msg,
                    "Your message contained more than one sentence.");
                return;
            }
        }

        /// <summary>
        ///     Responds with a timed message that the message was removed.
        /// </summary>
        /// <param name="message">
        ///     The message that gets removed.
        /// </param>
        /// <param name="reason">
        ///     The reason why it got removed.
        /// </param>
        /// <returns></returns>
        private static async Task DeleteMessageAsync(DiscordMessage message, string reason)
        {
            var embed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("Your message was removed!")
                .WithDescription(
                    $"**Reason:** {reason}\n\n" +
                    $"This message will destroy itself <t:{DateTimeOffset.UtcNow.AddSeconds(10).ToUnixTimeSeconds()}:R>.");

            var response = await message.RespondAsync(message.Author.Mention, embed);

            var timer = new System.Timers.Timer(new TimeSpan(0, 0, 10))
            {
                AutoReset = false
            };

            timer.Elapsed += async (sender, e) =>
            {
                await response.DeleteAsync("timed fanfic message");
            };
            timer.Start();

            ViverLog.FanficMessageDeleted(message, reason);
            await message.DeleteAsync($"Deleted message from fanfic\n[Reason] {reason}");
        }

        /// <summary>
        ///     A precompiled Regex to test for the only one sentence rule.
        /// </summary>
        /// <returns></returns>
        [GeneratedRegex("[.!?]{1}[^\n.!?*”]+")]
        private static partial Regex OneSentenceRegex();

        /// <summary>
        ///     A list of special edge cases that should be ignored.
        /// </summary>
        private static readonly string[] Exclusions = new string[]
        {
            // we all know what this is
            "I.M.P",
            "D.H.O.R.K.S",
            "C.H.E.R.U.B",
            // honorifics and titles
            "Mr.",
            "Ms.",
            "Mrs.",
            "Mx.",
            "Jr.",
            "Sr.",
            "Dr.",
            "Prof.",
            "Ph.",
            "M.",
            "Acct.",
            "Asst.",
            "CFO.",
            "CEO.",
            "CMO.",
            "EVP.",
            "PA.",
            "VP.",
            "Esq.",
            "Ing.",
            "Fr.",
            "Pr.",
            "Br.",
            "CS.",
            "C.S.",
            // corporations
            "Co.",
            "Ltd.",
            "Inc.",
            "Corp.",
            // countries etc.
            "U.S.A.",
            "U.S.",
            "E.U.",
            "U.K.",
            // commonly used
            "A.M.",
            "P.M.",
            "i.e.",
            "e.g.",
            "P.O.",
            "B.C.",
            "A.D.",
            "P.S.",
            "approx.",
            "appt.",
            "apt.",
            "A.S.A.P.",
            "B.Y.O.B.",
            "dept.",
            "D.I.Y.",
            "est.",
            "E.T.A.",
            "min.",
            "max.",
            "misc.",
            "no.",
            "R.S.V.P.",
            "tel.",
            "temp.",
            "vet.",
            "vs.",
            "etc.",
            "n.b.",
            "viz.",
            "et al.",
            "ibid.",
            "Q.E.D.",
            "C.V.",
            "sec.",
            "prim.",
            // geography
            "Ave.",
            "Blvd.",
            "St.",
            "Rd.",
            "Cyn.",
            "Ln."
        };
    }
}
