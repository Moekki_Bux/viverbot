﻿namespace ViverBot.Models
{
    public class DmHelpKey
    {
        public int Id { get; set; }
        public ulong GuildId { get; set; }
        public string Value { get; set; }
        public int? ResponseId { get; set; }
        public DmHelpResponse? DmHelpResponse { get; set; }

        public DmHelpKey()
        {
            Value = "";
        }

        public DmHelpKey(ulong guildId, string value)
        {
            GuildId = guildId;
            Value = value;
        }
    }
}
