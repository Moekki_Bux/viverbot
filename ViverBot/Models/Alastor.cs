﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ViverBot.Models
{
    public class Alastor
    {
        public ulong Id { get; set; }
        public DateTimeOffset LastFed { get; set; }
        public DateTimeOffset LastCrown { get; set; }
        public int HighestStreak { get; set; }
        public int CurrentStreak { get; set; }
        public int Crowns { get; set; }

        public Alastor()
        {
        }

        public Alastor(
            ulong id,
            DateTimeOffset lastFed,
            DateTimeOffset lastCrown,
            int highestStreak,
            int currentStreak,
            int crowns)
        {
            Id = id;
            LastFed = lastFed;
            LastCrown = lastCrown;
            HighestStreak = highestStreak;
            CurrentStreak = currentStreak;
            Crowns = crowns;
        }
    }
}
