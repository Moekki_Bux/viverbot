﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ViverBot.Models
{
    public class DmHelpResponse
    {
        public int Id { get; set; }
        public ulong GuildId { get; set; }
        public string Text { get; set; }
        public ICollection<DmHelpKey> DmHelpKeys { get; set; } = new List<DmHelpKey>();

        public DmHelpResponse()
        {
            Text = "";
        }

        public DmHelpResponse(ulong guildId, string text)
        {
            GuildId = guildId;
            Text = text;
        }
    }
}
