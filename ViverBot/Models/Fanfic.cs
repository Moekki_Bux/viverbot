﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ViverBot.Models
{
    /// <summary>
    ///     Represents a fanfic.
    /// </summary>
    public class Fanfic
    {
        /// <summary>
        ///     The id of the fanfic.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     The name of the fanfic.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     The id of the author.
        /// </summary>
        public ulong AuthorId { get; set; }

        /// <summary>
        ///     The id of the thread where the fanfic is located.
        /// </summary>
        public ulong ThreadId { get; set; }

        /// <summary>
        ///     The id of the channel where the thread is located.
        /// </summary>
        public ulong ChannelId { get; set; }

        /// <summary>
        ///     The id of the guild.
        /// </summary>
        public ulong GuildId { get; set; }

        /// <summary>
        ///     If the fanfic is temporarily locked, this is the id of the message why it is locked.
        /// </summary>
        public ulong? LockMessageId { get; set; }

        /// <summary>
        ///     Whether or not the fanfic is active.
        /// </summary>
        public bool IsActive { get; set; }

        public Fanfic()
        {
            Name = "";
        }

        public Fanfic(string name, ulong authorId, ulong threadId, ulong channelId, ulong guildId)
        {
            Name = name;
            AuthorId = authorId;
            ThreadId = threadId;
            ChannelId = channelId;
            GuildId = guildId;
            IsActive = true;
        }
    }
}
