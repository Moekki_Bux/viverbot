﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ViverBot.Migrations
{
    /// <inheritdoc />
    public partial class UpdateTableNames : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AutoReactChannels",
                table: "AutoReactChannels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DmHelpResponse",
                table: "DmHelpResponse");

            migrationBuilder.RenameTable(
                name: "AutoReactChannels",
                newName: "autoreactchannels");

            migrationBuilder.RenameTable(
                name: "DmHelpResponse",
                newName: "dmhelpresponses");

            migrationBuilder.RenameIndex(
                name: "IX_AutoReactChannels_guildid",
                table: "autoreactchannels",
                newName: "IX_autoreactchannels_guildid");

            migrationBuilder.RenameIndex(
                name: "IX_DmHelpResponse_guildid_key",
                table: "dmhelpresponses",
                newName: "IX_dmhelpresponses_guildid_key");

            migrationBuilder.AddPrimaryKey(
                name: "PK_autoreactchannels",
                table: "autoreactchannels",
                column: "id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dmhelpresponses",
                table: "dmhelpresponses",
                column: "id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_autoreactchannels",
                table: "autoreactchannels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dmhelpresponses",
                table: "dmhelpresponses");

            migrationBuilder.RenameTable(
                name: "autoreactchannels",
                newName: "AutoReactChannels");

            migrationBuilder.RenameTable(
                name: "dmhelpresponses",
                newName: "DmHelpResponse");

            migrationBuilder.RenameIndex(
                name: "IX_autoreactchannels_guildid",
                table: "AutoReactChannels",
                newName: "IX_AutoReactChannels_guildid");

            migrationBuilder.RenameIndex(
                name: "IX_dmhelpresponses_guildid_key",
                table: "DmHelpResponse",
                newName: "IX_DmHelpResponse_guildid_key");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AutoReactChannels",
                table: "AutoReactChannels",
                column: "id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DmHelpResponse",
                table: "DmHelpResponse",
                column: "id");
        }
    }
}
