﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ViverBot.Migrations
{
    /// <inheritdoc />
    public partial class DmHelpSplitKeyAndResponse : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_dmhelpresponses_guildid_key",
                table: "dmhelpresponses");

            migrationBuilder.DropColumn(
                name: "key",
                table: "dmhelpresponses");

            migrationBuilder.RenameColumn(
                name: "responsetext",
                table: "dmhelpresponses",
                newName: "text");

            migrationBuilder.CreateTable(
                name: "dmhelpkeys",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    guildid = table.Column<ulong>(type: "bigint unsigned", nullable: false),
                    value = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ResponseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dmhelpkeys", x => x.id);
                    table.ForeignKey(
                        name: "FK_dmhelpkeys_dmhelpresponses_ResponseId",
                        column: x => x.ResponseId,
                        principalTable: "dmhelpresponses",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_dmhelpresponses_guildid",
                table: "dmhelpresponses",
                column: "guildid");

            migrationBuilder.CreateIndex(
                name: "IX_dmhelpkeys_guildid_value",
                table: "dmhelpkeys",
                columns: new[] { "guildid", "value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_dmhelpkeys_ResponseId",
                table: "dmhelpkeys",
                column: "ResponseId");

            migrationBuilder.CreateIndex(
                name: "IX_dmhelpkeys_value",
                table: "dmhelpkeys",
                column: "value");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dmhelpkeys");

            migrationBuilder.DropIndex(
                name: "IX_dmhelpresponses_guildid",
                table: "dmhelpresponses");

            migrationBuilder.RenameColumn(
                name: "text",
                table: "dmhelpresponses",
                newName: "responsetext");

            migrationBuilder.AddColumn<string>(
                name: "key",
                table: "dmhelpresponses",
                type: "varchar(255)",
                nullable: false,
                defaultValue: "")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_dmhelpresponses_guildid_key",
                table: "dmhelpresponses",
                columns: new[] { "guildid", "key" },
                unique: true);
        }
    }
}
