﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Newtonsoft.Json;
using System.Text;

namespace ViverBot
{
    /// <summary>
    ///     Represents the settings for the ViverBot.
    /// </summary>
    public class ViverSettings
    {
        /// <summary>
        ///     The token to identify the client.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        ///     The connection string to the database.
        /// </summary>
        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }

        /// <summary>
        ///     The default interactivity timeout.
        /// </summary>
        [JsonProperty("timeout")]
        public int Timeout { get; set; }

        /// <summary>
        ///     The default embed color for the responses.
        /// </summary>
        [JsonProperty("embedColor")]
        public string EmbedColor { get; set; }

        /// <summary>
        ///     The minimum log level.
        /// </summary>
        [JsonProperty("logLevel")]
        public string LogLevel { get; set; }

        /// <summary>
        ///     The id of the support server for the bot.
        /// </summary>
        [JsonProperty("supportServerId")]
        public string SupportServerId { get; set; }

        [JsonProperty("fanficNumberInStatus")]
        public bool FanficNumberInStatus { get; set; }

        /// <summary>
        ///     Creates a new settings instance with default values. Caution: Some default values are for informational
        ///     purposes and are not parseable.
        /// </summary>
        public ViverSettings()
        {
            Token = "insert your bot token here. For more information, take a look into this: https://discord.com/developers/docs/getting-started";
            ConnectionString = "insert your MySQL connection string here.";
            Timeout = 5;
            EmbedColor = "#23272A";
            LogLevel = "information";
            SupportServerId = "insert the id of your support server here.";
            FanficNumberInStatus = true;
        }

        /// <summary>
        ///     Searches for a settings file and returns its path. If no settings file is found, a new one with
        ///     default values will be created and a exception will be thrown.
        /// </summary>
        /// <param name="path">
        ///     The directory where the settings file is supposed to be.
        /// </param>
        /// <returns>
        ///     The path of the settings file, if found.
        /// </returns>
        /// <exception cref="IOException"></exception>
        public static string GetSettingsFile(string path)
        {
            var file = "settings.json";
            var filePath = Path.Combine(path, file);
            if (File.Exists(filePath))
            {
                return filePath;
            }
            else
            {
                var exMessage = "No settings file was found! ";
                if (CreateSettings(path, file))
                {
                    exMessage += "A new settings file was created at " + path;
                }
                else
                {
                    exMessage += "A new settings file cannot be created!";
                }

                throw new IOException(exMessage);
            }
        }

        /// <summary>
        ///     Creates a new settings file with default values.
        /// </summary>
        /// <param name="path">
        ///     The directory path.
        /// </param>
        /// <param name="file">
        ///     The name of the new settings file.
        /// </param>
        /// <returns>
        ///     true if the settings file was created, otherwise false.
        /// </returns>
        private static bool CreateSettings(string path, string file)
        {
            try
            {
                Directory.CreateDirectory(path);

                var json = JsonConvert.SerializeObject(new ViverSettings());

                string filePath = Path.Combine(path, file);
                using var fs = File.Create(filePath);
                using var sw = new StreamWriter(fs, Encoding.UTF8);
                sw.Write(json);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
