﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ViverBot.Models;

namespace ViverBot.Context
{
    /// <summary>
    ///     Represents the database as a ORM model.
    /// </summary>
    public class ViverContext : DbContext
    {
        public DbSet<Fanfic> Fanfics { get; set; }
        public DbSet<Alastor> Alastors { get; set; }
        public DbSet<AutoReactChannel> AutoReactChannels { get; set; }
        public DbSet<DmHelpResponse> DmHelpResponses { get; set; }
        public DbSet<DmHelpKey> DmHelpKeys { get; set; }

        private readonly IConfiguration? _configuration;

        public ViverContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ViverContext(DbContextOptions<ViverContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Fanfic>(entity =>
            {
                entity.HasKey(f => f.Id);

                entity.ToTable("fanfics");

                entity.Property(f => f.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(f => f.Name)
                    .HasColumnName("name");

                entity.Property(f => f.AuthorId)
                    .HasColumnName("authorid");

                entity.Property(f => f.ThreadId)
                    .HasColumnName("threadid");

                entity.Property(f => f.ChannelId)
                    .HasColumnName("channelid");

                entity.Property(f => f.GuildId)
                    .HasColumnName("guildid");

                entity.Property(f => f.LockMessageId)
                    .HasColumnName("lockmessageid");

                entity.Property(f => f.IsActive)
                    .HasColumnName("isactive");

                entity.HasIndex(f => f.Name);
                entity.HasIndex(f => f.AuthorId);
                entity.HasIndex(f => f.ThreadId).IsUnique();
                entity.HasIndex(f => f.ChannelId);
                entity.HasIndex(f => f.GuildId);
                entity.HasIndex(f => f.IsActive);
            });

            modelBuilder.Entity<Alastor>(entity =>
            {
                entity.HasKey(a => a.Id);

                entity.ToTable("alastor");

                entity.Property(a => a.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(a => a.LastFed)
                    .HasConversion<long>()
                    .HasColumnName("lastfed");

                entity.Property(a => a.LastCrown)
                    .HasConversion<long>()
                    .HasColumnName("lastcrown");

                entity.Property(a => a.HighestStreak)
                    .HasColumnName("higheststreak");

                entity.Property(a => a.CurrentStreak)
                    .HasColumnName("streak");

                entity.Property(a => a.Crowns)
                    .HasColumnName("crowns");
            });

            modelBuilder.Entity<AutoReactChannel>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.ToTable("autoreactchannels");

                entity.Property(c => c.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(c => c.ChannelId)
                    .HasColumnName("channelid");

                entity.Property(c => c.GuildId)
                    .HasColumnName("guildid");

                entity.Property(c => c.Emojis)
                    .HasColumnName("emojis");

                entity.HasIndex(c => c.GuildId);
            });

            modelBuilder.Entity<DmHelpResponse>(entity =>
            {
                entity.HasKey(r => r.Id);

                entity.ToTable("dmhelpresponses");

                entity.Property(r => r.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(r => r.GuildId)
                    .HasColumnName("guildid");

                entity.Property(r => r.Text)
                    .HasColumnName("text");

                entity.HasIndex(r => r.GuildId);

                entity.HasMany(r => r.DmHelpKeys)
                    .WithOne(k => k.DmHelpResponse)
                    .HasForeignKey(k => k.ResponseId)
                    .IsRequired(false);

                entity.HasIndex(r => r.GuildId);
            });

            modelBuilder.Entity<DmHelpKey>(entity =>
            {
                entity.HasKey(k => k.Id);

                entity.ToTable("dmhelpkeys");

                entity.Property(k => k.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(k => k.GuildId)
                    .HasColumnName("guildid");

                entity.Property(k => k.Value)
                    .HasColumnName("value");

                entity.HasIndex(k => new { k.GuildId, k.Value }).IsUnique();
                entity.HasIndex(k => k.Value);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured && _configuration is not null)
            {
                var connectionString = _configuration["connectionString"];
                optionsBuilder.UseMySql(
                    connectionString,
                    ServerVersion.AutoDetect(connectionString),
                    sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(5);
                    });
            }

            optionsBuilder.ConfigureWarnings(configBuilder =>
                configBuilder
                    .Ignore(RelationalEventId.CommandExecuted)
                );
        }
    }
}