﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.Entities;
using Microsoft.Extensions.Options;
using System.Diagnostics;
using System.Reflection;
using ViverBot.Models;

namespace ViverBot.Services
{
    /// <summary>
    ///     Defines methods to create embeds and other message content types.
    /// </summary>
    public interface IResponseService
    {
        #region Core
        /// <summary>
        ///     A <see cref="string"/> that displays the ping of the bot.
        /// </summary>
        /// <param name="client">
        ///     The client of the bot.
        /// </param>
        /// <returns>
        ///     A <see cref="string"/> with the ping.
        /// </returns>
        string PingContent(DiscordClient client);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that holds information about the bot.
        /// </summary>
        /// <param name="client">
        ///     <inheritdoc cref="PingContent(DiscordClient)" path="/param[@name='client']"/>
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that holds information about the bot.
        /// </returns>
        DiscordEmbed AboutEmbed(DiscordClient client);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows the current user count without bots of the guild.
        /// </summary>
        /// <param name="memberCount">
        ///     The current user count in the server excluding bots.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows the current user count without bots of this guild.
        /// </returns>
        DiscordEmbed UserCountEmbedAsync(int userCount, int botCount, DiscordRole? role = null);

        /// <summary>
        ///     Implementation of RFC2324.
        /// </summary>
        /// <returns>
        ///     <c>Error 418! I'm a teapot!</c>
        /// </returns>
        DiscordEmbed CoffeeEmbed();
        #endregion

        #region Manage
        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that the activity was updated.
        /// </summary>
        /// <param name="activity">
        ///     The new activity.
        /// </param>
        /// <param name="status">
        ///     The new status.
        /// </param>
        /// <param name="member">
        ///     The user who updated the activity.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that the activity was updated.
        /// </returns>
        DiscordEmbed ActivityUpdatedEmbed(DiscordActivity activity, UserStatus status, DiscordMember member);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that the status was updated.
        /// </summary>
        /// <param name="status">
        ///     The new status.
        /// </param>
        /// <param name="member">
        ///     The user who updated the status.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that the status was updated.
        /// </returns>
        DiscordEmbed StatusUpdatedEmbed(UserStatus status, DiscordMember member);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that a reconnect was requested.
        /// </summary>
        /// <param name="member">
        ///     The user who requested the reconnect.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that a reconnect was requested.
        /// </returns>
        DiscordEmbed ReconnectEmbed(DiscordMember member);
        #endregion

        #region Fanfic
        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows an error that there is already a active fanfic.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows a error that there is already a active fanfic.
        /// </returns>
        DiscordEmbed AlreadyActiveFanficEmbed();

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows an error that there is already a fanfic with
        ///     that name in this channel.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows an error that there is already a fanfic with
        ///     that name in this channel.
        /// </returns>
        DiscordEmbed SameNameFanficEmbed();

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that a new fanfic was started.
        /// </summary>
        /// <param name="member">
        ///     The member who started the fanfic.
        /// </param>
        /// <param name="name">
        ///     The name of the new fanfic.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that a new fanfic was started.
        /// </returns>
        DiscordEmbed StartFanficEmbed(DiscordMember member, string name);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that no fanfic was found.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that no fanfic was found.
        /// </returns>
        DiscordEmbed NoFanficEmbed();

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that the current fanfic was ended.
        /// </summary>
        /// <param name="member">
        ///     The member who ended the fanfic.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that the current fanfic was ended.
        /// </returns>
        DiscordEmbed EndFanficEmbed(DiscordMember member);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that the current user isn't authorized to end
        ///     the current fanfic.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that the current user isn't authorized to end
        ///     the current fanfic.
        /// </returns>
        DiscordEmbed UnauthorizedFanficEmbed();

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows a error while retrieving a fanfic.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows a error while retrieving a fanfic.
        /// </returns>
        DiscordEmbed ErrorShowFanficEmbed();
        #endregion

        #region FanficThread
        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows that a new fanfic
        ///     thread was started.
        /// </summary>
        /// <param name="member">
        ///     The member who started the fanfic thread.
        /// </param>
        /// <param name="name">
        ///     The name of the fanfic.
        /// </param>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows that a new fanfic
        ///     thread was started.
        /// </returns>
        DiscordEmbed StartFanficThreadEmbed(DiscordMember member, string name);

        /// <summary>
        ///     A <see cref="DiscordEmbed"/> that shows a very bad error.
        /// </summary>
        /// <returns>
        ///     A <see cref="DiscordEmbed"/> that shows a very bad error.
        /// </returns>
        DiscordEmbed OnoEmbed();
        #endregion

        #region Alastor
        DiscordEmbed AlastorEmbed(DiscordMember member, Alastor alastor, string title, string gif);
        #endregion
    }

    /// <summary>
    ///     Service to create embeds and message contents.
    /// </summary>
    public class ResponseService : IResponseService
    {
        private static DiscordColor _embedColor;

        public ResponseService(IOptions<ViverSettings> settings)
        {
            _embedColor = new DiscordColor(settings.Value.EmbedColor);
        }

        #region Core
        public string PingContent(DiscordClient client)
        {
            return $"Pong! `{client.Ping}ms`";
        }

        public DiscordEmbed AboutEmbed(DiscordClient client)
        {
            var env = Assembly.GetExecutingAssembly()
                .GetCustomAttribute<AssemblyConfigurationAttribute>()?
                .Configuration;

            var proc = Process.GetCurrentProcess();
            var owners = client.CurrentApplication.Owners;
            return new DiscordEmbedBuilder()
                .WithColor(_embedColor)
                .WithTitle(client.CurrentUser.Username)
                .WithDescription("A simple bot to monitor and enforce the fanfic rules in a thread")
                .AddField("Developer", string.Join(", ", owners.Select(o => o.Mention)), true)
                .AddField("Source Code",
                    "[Gitlab](https://gitlab.com/Moekki_Bux/viverbot)", true)
                .AddField("License",
                    "[AGPLv3 License](https://choosealicense.com/licenses/agpl-3.0/)", true)
                .AddField("Bot Version",
                    $"```{Assembly.GetExecutingAssembly()?.GetName().Version?.ToString(3)}```", true)
                .AddField("Threads",
                    $"```{proc.Threads.Count}```", true)
                .AddField("RAM Usage",
                    $"```{proc.PrivateMemorySize64 / 1000000} MB```", true)
                //.AddField("Framework",
                //    $"```{Assembly.GetEntryAssembly()?.GetCustomAttribute<TargetFrameworkAttribute>()?.FrameworkDisplayName}```", true)
                .AddField("DSharpPlus Version",
                    $"```{client.VersionString}```", true)
                .AddField("Gateway Version",
                    $"```{client.GatewayVersion}```", true)
                .AddField("Environment",
                    $"```{env}```", true)
                //.AddField("Shards",
                //    $"```{client.ShardCount}```", true)
                .AddField("Last Restart",
                    $"<t:{((DateTimeOffset)proc.StartTime).ToUnixTimeSeconds()}:R>", true)
                .WithThumbnail(client.CurrentUser.AvatarUrl);
        }

        public DiscordEmbed UserCountEmbedAsync(int userCount, int botCount, DiscordRole? role = null)
        {
            string title = role is null ? "Members" : "Members with role " + role.Name;
            var color = role is null ? _embedColor : role.Color;
            return new DiscordEmbedBuilder()
                .WithColor(color)
                .WithTitle(title)
                .WithDescription(
                    $"**Users:** {userCount}\n" +
                    $"**Bots:** {botCount}\n" +
                    $"**Total:** {userCount + botCount}");
        }

        public DiscordEmbed CoffeeEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("Error 418 I'm a teapot")
                .WithDescription("For more information see https://www.rfc-editor.org/rfc/rfc2324");
        }
        #endregion

        #region Manage
        public DiscordEmbed ActivityUpdatedEmbed(DiscordActivity activity, UserStatus status, DiscordMember member)
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Yellow)
                .WithAuthor(member.DisplayName, null, member.GetGuildAvatarUrl(ImageFormat.Auto))
                .WithTitle("Activity was updated")
                .AddField("Activity", $"**[Type]** {activity.ActivityType}\n**[Name]** {activity.Name}\n**[Status]** {status}");
        }

        public DiscordEmbed StatusUpdatedEmbed(UserStatus status, DiscordMember member)
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Yellow)
                .WithAuthor(member.DisplayName, null, member.GetGuildAvatarUrl(ImageFormat.Auto))
                .WithTitle("Status was updated")
                .AddField("Status", $"**[Status]** {status}");
        }

        public DiscordEmbed ReconnectEmbed(DiscordMember member)
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Yellow)
                .WithAuthor(member.DisplayName, null, member.GetGuildAvatarUrl(ImageFormat.Auto))
                .WithTitle("Reconnecting");
        }
        #endregion

        #region Fanfic
        public DiscordEmbed AlreadyActiveFanficEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("There is already a fanfic going on!")
                .WithDescription("Please wait until the current fanfic is finished.");
        }

        public DiscordEmbed SameNameFanficEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("There is already a fanfic with that name!")
                .WithDescription("Please try again using a different name.");
        }

        public DiscordEmbed StartFanficEmbed(DiscordMember member, string name)
        {
            return new DiscordEmbedBuilder()
                .WithColor(_embedColor)
                .WithTitle($"{member.DisplayName} started a new fanfic! The title is \"{name}\"")
                .WithDescription("Here are the rules:\n" +
                "- Only one sentence per message\n" +
                "- The person who sent the most recent message is not allowed to send the next message\n" +
                "- A second sentence is allowed if it is enclosed in quotation marks");
        }

        public DiscordEmbed NoFanficEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("There is currently no fanfic that could be stopped!");
        }

        public DiscordEmbed EndFanficEmbed(DiscordMember member)
        {
            return new DiscordEmbedBuilder()
                .WithColor(_embedColor)
                .WithTitle($"{member.DisplayName} ended the current fanfic!");
        }

        public DiscordEmbed UnauthorizedFanficEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("Only the author or a moderator can stop a fanfic!");
        }

        public DiscordEmbed ErrorShowFanficEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("Something went wrong! Please make sure the fanfic exists.");
        }
        #endregion

        #region FanficThread
        public DiscordEmbed StartFanficThreadEmbed(DiscordMember member, string name)
        {
            return new DiscordEmbedBuilder()
                .WithColor(_embedColor)
                .WithAuthor(member.DisplayName, null, member.GetGuildAvatarUrl(ImageFormat.Auto))
                .WithTitle(name)
                .WithDescription("Here are the rules:\n" +
                "- Only one sentence per message\n" +
                "- The person who sent the most recent message is not allowed to send the next message");
        }

        public DiscordEmbed OnoEmbed()
        {
            return new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Red)
                .WithTitle("Something bad happened!")
                .WithDescription("If you see this, please contact either the developer directly or a server staff member.\n" +
                    "Don't worry, you didn't break anything, this is a fault from the developer's side.\n\n" +
                    "I apologize for the inconvenience.");
        }
        #endregion

        #region Alastor
        public DiscordEmbed AlastorEmbed(DiscordMember member, Alastor alastor, string title, string gif)
        {
            return new DiscordEmbedBuilder()
                .WithColor(_embedColor)
                .WithAuthor(title, null, member.AvatarUrl)
                .WithImageUrl(gif)
                .AddField("Streak", $"{alastor.CurrentStreak}", true)
                .AddField("Reset", $"<t:{alastor.LastFed.AddDays(1).ToUnixTimeSeconds()}:R>", true);
        }
        #endregion
    }
}
