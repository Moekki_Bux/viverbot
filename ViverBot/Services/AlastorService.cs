﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;
using System.IO;
using ViverBot.Context;
using ViverBot.Models;

namespace ViverBot.Services
{
    public interface IAlastorService
    {
        Task<Alastor?> GetAlastorAsync(ulong id);
        Task<Alastor> AddAlastorAsync(ulong id);
        Task UpdateAlastorAsync(Alastor alastorFeeder);
        Task UpdateLastFedAsync(ulong id, DateTimeOffset lastFed);
        Task UpdateLastCrownAsync(ulong id, DateTimeOffset lastCrown);
        Task UpdateHighestStreakAsync(ulong id, int streak);
        Task UpdateCurrentStreakAsync(ulong id, int streak);
        Task UpdateCrownsAsync(ulong id, int crowns);
    }

    public class AlastorService : IAlastorService
    {
        private readonly ViverContext _dbContext;

        public AlastorService(ViverContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Alastor?> GetAlastorAsync(ulong id)
        {
            return await _dbContext.Alastors
                .Where(a => a.Id == id)
                .SingleOrDefaultAsync();
        }

        public async Task<Alastor> AddAlastorAsync(ulong id)
        {
            var alastor = new Alastor(id, DateTimeOffset.UtcNow, DateTimeOffset.UtcNow, 1, 1, 9999);
            await _dbContext.Alastors.AddAsync(alastor);
            await _dbContext.SaveChangesAsync();

            return alastor;
        }

        public async Task UpdateAlastorAsync(Alastor alastorFeeder)
        {
            _dbContext.Alastors.Update(alastorFeeder);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateLastFedAsync(ulong id, DateTimeOffset lastFed)
        {
            var alastorFeeder = await GetAlastorAsync(id);
            if (alastorFeeder is not null)
            {
                alastorFeeder.LastFed = lastFed;
                await UpdateAlastorAsync(alastorFeeder);
            }
        }

        public async Task UpdateLastCrownAsync(ulong id, DateTimeOffset lastCrown)
        {
            var alastorFeeder = await GetAlastorAsync(id);
            if (alastorFeeder is not null)
            {
                alastorFeeder.LastCrown = lastCrown;
                await UpdateAlastorAsync(alastorFeeder);
            }
        }

        public async Task UpdateHighestStreakAsync(ulong id, int streak)
        {
            var alastorFeeder = await GetAlastorAsync(id);
            if (alastorFeeder is not null)
            {
                alastorFeeder.HighestStreak = streak;
                await UpdateAlastorAsync(alastorFeeder);
            }
        }

        public async Task UpdateCurrentStreakAsync(ulong id, int streak)
        {
            var alastorFeeder = await GetAlastorAsync(id);
            if (alastorFeeder is not null)
            {
                alastorFeeder.CurrentStreak = streak;
                await UpdateAlastorAsync(alastorFeeder);
            }
        }

        public async Task UpdateCrownsAsync(ulong id, int crowns)
        {
            var alastorFeeder = await GetAlastorAsync(id);
            if (alastorFeeder is not null)
            {
                alastorFeeder.Crowns = crowns;
                await UpdateAlastorAsync(alastorFeeder);
            }
        }
    }
}
