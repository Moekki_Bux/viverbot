﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;
using ViverBot.Context;
using ViverBot.Models;

namespace ViverBot.Services.AutoReactServices
{
    public interface IAutoReactService
    {
        Task<List<AutoReactChannel>> GetAutoReactsAsync();

        Task<List<AutoReactChannel>> GetAutoReactsAsync(ulong guildId);

        Task<AutoReactChannel?> GetAutoReactAsync(ulong guildId, ulong channelId);

        Task AddAutoReactAsync(ulong guildId, ulong channelId, string emojis);

        Task UpdateAutoReactAsync(AutoReactChannel autoReact);

        Task<bool> RemoveAutoReactAsync(ulong guildId, ulong channelId);
    }

    public class AutoReactService : IAutoReactService
    {
        private readonly ViverContext _dbContext;

        public AutoReactService(ViverContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<AutoReactChannel>> GetAutoReactsAsync()
        {
            return await _dbContext.AutoReactChannels
                .ToListAsync();
        }

        public async Task<List<AutoReactChannel>> GetAutoReactsAsync(ulong guildId)
        {
            return await _dbContext.AutoReactChannels
                .Where(c => c.GuildId == guildId)
                .ToListAsync();
        }

        public async Task<AutoReactChannel?> GetAutoReactAsync(ulong guildId, ulong channelId)
        {
            return await _dbContext.AutoReactChannels
                .Where(c => c.GuildId == guildId &&
                            c.ChannelId == channelId)
                .SingleOrDefaultAsync();
        }

        public async Task AddAutoReactAsync(ulong guildId, ulong channelId, string emojis)
        {
            var autoReact = new AutoReactChannel(channelId, guildId, emojis);
            await _dbContext.AutoReactChannels.AddAsync(autoReact);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAutoReactAsync(AutoReactChannel autoReact)
        {
            _dbContext.AutoReactChannels.Update(autoReact);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> RemoveAutoReactAsync(ulong guildId, ulong channelId)
        {
            var autoReact = await GetAutoReactAsync(guildId, channelId);
            if (autoReact is not null)
            {
                _dbContext.AutoReactChannels.Remove(autoReact);
                await _dbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
