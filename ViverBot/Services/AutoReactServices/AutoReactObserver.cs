﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.AsyncEvents;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using System.Collections.Concurrent;
using ViverBot.Extensions;
using ViverBot.Logging;

namespace ViverBot.Services.AutoReactServices
{
    /// <summary>
    ///     Observer for managing event handlers for the auto reactions.
    /// </summary>
    public class AutoReactObserver
    {
        private readonly ConcurrentDictionary<ulong, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>> _observers;

        public AutoReactObserver()
        {
            _observers = new ConcurrentDictionary<ulong, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>>();
        }

        /// <summary>
        ///     Creates and adds a new event handler to the observer and subscribes it to the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="channel">
        ///     The channel to listen to.
        /// </param>
        /// <returns>
        ///     Whether the subscribe was successful.
        /// </returns>
        public bool Subscribe(DiscordClient client, DiscordChannel channel, List<string> reactions)
        {
            var emojis = new List<DiscordEmoji>();
            foreach (var reaction in reactions)
            {
                var emoji = reaction.GetEmoji(client);
                if (emoji != null)
                {
                    emojis.Add(emoji);
                }
            }

            try
            {
                if (emojis.Count > 0 &&
                    _observers.TryAdd(channel.Id,
                    (sender, e) => AutoReactAsync(sender, e, channel, emojis)))
                {
                    client.MessageCreated += _observers[channel.Id];
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.SubscribeToThreadErrored(ex.Message, channel.Id);
                return false;
            }
        }

        /// <summary>
        ///     Removes a event handler from the observer and unsubscribes it from the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="channelId">
        ///     The id of the channel.
        /// </param>
        /// <returns>
        ///     Whether the unsubscribe was successful.
        /// </returns>
        public bool Unsubscribe(DiscordClient client, ulong channelId)
        {
            try
            {
                if (_observers.TryRemove(channelId, out AsyncEventHandler<DiscordClient, MessageCreateEventArgs>? value))
                {
                    client.MessageCreated -= value;
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.UnsubscribeFromThreadErrored(ex.Message, channelId);
                return false;
            }
        }

        /// <summary>
        ///     Retrieves the current count of active event handlers managed by the observer.
        /// </summary>
        /// <returns>
        ///     The count of event handlers.
        /// </returns>
        public int GetCountOfObservers()
        {
            return _observers.Count;
        }

        private static async Task AutoReactAsync(
            DiscordClient sender,
            MessageCreateEventArgs e,
            DiscordChannel channel,
            List<DiscordEmoji> reactions)
        {
            if (e.Channel.Id != channel.Id)
            {
                return;
            }

            // TODO: This is dodgy, think of something else.
            Thread.Sleep(700);
            if (e.Message.Content.StartsWith("!spc", StringComparison.OrdinalIgnoreCase))
            {
                if (e.Message.Attachments.Count > 0 ||
                    (!e.Message.Author.IsBot && e.Message.Embeds.Count > 0))
                {
                    foreach (var reaction in reactions)
                    {
                        await e.Message.CreateReactionAsync(reaction);
                        Thread.Sleep(300);
                    }
                }
                else
                {
                    ViverLog.Warning("!spc command was issued, but no attachment was found!");
                }
            }
        }
    }
}
