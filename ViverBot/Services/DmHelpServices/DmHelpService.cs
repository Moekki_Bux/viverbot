﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using ViverBot.Context;
using ViverBot.Extensions;
using ViverBot.Models;

namespace ViverBot.Services.DmHelpServices
{
    public interface IDmHelpService
    {
        Task AddDmHelpAsync(ulong guildId, string key, string text);
        Task<bool> DeleteDmHelpAsync(DmHelpKey dmHelpKey);

        Task<List<DmHelpResponse>> GetDmHelpResponsesAsync();
        Task<List<DmHelpResponse>> GetDmHelpResponsesAsync(ulong guildId);
        Task<DmHelpResponse?> GetDmHelpResponseAsync(ulong guildId, string key);
        Task AddDmHelpResponseAsync(ulong guildId, string text);
        Task UpdateDmHelpResponseAsync(DmHelpResponse dmHelpResponse);
        Task<bool> DeleteDmHelpResponseAsync(ulong guildId, string key);

        Task<List<DmHelpKey>> GetDmHelpKeysAsync();
        Task<List<DmHelpKey>> GetDmHelpKeysAsync(ulong guildId);
        Task<DmHelpKey?> GetDmHelpKeyAsync(ulong guildId, string key);
        Task AddDmHelpKeyAsync(ulong guildId, string key);
        Task UpdateDmHelpKeyAsync(DmHelpKey dmHelpKey);
        Task<bool> DeleteDmHelpKeyAsync(ulong guildId, string key);
    }

    public partial class DmHelpService : IDmHelpService
    {
        private readonly ViverContext _dbContext;

        public DmHelpService(ViverContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddDmHelpAsync(ulong guildId, string key, string text)
        {
            key = key.ToKeyRepresentation();
            var dmHelpKey = new DmHelpKey(guildId, key);
            var dmHelpResponse = new DmHelpResponse(guildId, text);
            dmHelpResponse.DmHelpKeys.Add(dmHelpKey);
            await _dbContext.DmHelpResponses.AddAsync(dmHelpResponse);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteDmHelpAsync(DmHelpKey dmHelpKey)
        {
            if (dmHelpKey.DmHelpResponse is not null)
            {
                _dbContext.DmHelpResponses.Remove(dmHelpKey.DmHelpResponse);
                _dbContext.DmHelpKeys.Remove(dmHelpKey);
                await _dbContext.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<List<DmHelpResponse>> GetDmHelpResponsesAsync()
        {
            return await _dbContext.DmHelpResponses
                .Include(r => r.DmHelpKeys)
                .ToListAsync();
        }

        public async Task<List<DmHelpResponse>> GetDmHelpResponsesAsync(ulong guildId)
        {
            return await _dbContext.DmHelpResponses
                .Include(r => r.DmHelpKeys)
                .Where(r => r.GuildId == guildId)
                .ToListAsync();
        }

        public async Task<DmHelpResponse?> GetDmHelpResponseAsync(ulong guildId, string key)
        {
            key = key.ToKeyRepresentation();
            return await _dbContext.DmHelpResponses
                .Include(r => r.DmHelpKeys)
                .Where(r => r.GuildId == guildId && 
                            r.DmHelpKeys.Any(k => k.Value == key))
                .SingleOrDefaultAsync();
        }

        public async Task AddDmHelpResponseAsync(ulong guildId, string text)
        {
            var response = new DmHelpResponse(guildId, text);
            await _dbContext.DmHelpResponses.AddAsync(response);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateDmHelpResponseAsync(DmHelpResponse dmHelpResponse)
        {
            _dbContext.DmHelpResponses.Update(dmHelpResponse);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteDmHelpResponseAsync(ulong guildId, string key)
        {
            var dmHelpResponse = await GetDmHelpResponseAsync(guildId, key.ToKeyRepresentation());
            if (dmHelpResponse is not null)
            {
                _dbContext.DmHelpResponses.Remove(dmHelpResponse);
                await _dbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<List<DmHelpKey>> GetDmHelpKeysAsync()
        {
            return await _dbContext.DmHelpKeys
                .Include(k => k.DmHelpResponse)
                .ToListAsync();
        }

        public async Task<List<DmHelpKey>> GetDmHelpKeysAsync(ulong guildId)
        {
            return await _dbContext.DmHelpKeys
                .Include (k => k.DmHelpResponse)
                .Where(k => k.GuildId == guildId)
                .ToListAsync();
        }

        public async Task<DmHelpKey?> GetDmHelpKeyAsync(ulong guildId, string key)
        {
            key = key.ToKeyRepresentation();
            return await _dbContext.DmHelpKeys
                .Include(k => k.DmHelpResponse)
                .Where(k => k.GuildId == guildId &&
                            k.Value == key)
                .SingleOrDefaultAsync();
        }

        public async Task AddDmHelpKeyAsync(ulong guildId, string key)
        {
            var dmHelpKey = new DmHelpKey(guildId, key.ToKeyRepresentation());
            await _dbContext.DmHelpKeys.AddAsync(dmHelpKey);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateDmHelpKeyAsync(DmHelpKey dmHelpKey)
        {
            _dbContext.DmHelpKeys.Update(dmHelpKey);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteDmHelpKeyAsync(ulong guildId, string key)
        {
            var dmHelpKey = await GetDmHelpKeyAsync(guildId, key.ToKeyRepresentation());
            if (dmHelpKey is not null)
            {
                _dbContext.DmHelpKeys.Remove(dmHelpKey);
                await _dbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
