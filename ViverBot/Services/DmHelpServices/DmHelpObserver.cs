﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus.AsyncEvents;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus;
using System.Collections.Concurrent;
using ViverBot.Handlers;
using ViverBot.Logging;
using ViverBot.Models;
using Serilog;
using ViverBot.Extensions;

namespace ViverBot.Services.DmHelpServices
{
    /// <summary>
    ///     Observer for managing event handlers for dm help listeners.
    /// </summary>
    public class DmHelpObserver
    {
        private readonly ConcurrentDictionary<int, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>> _observers;

        public DmHelpObserver()
        {
            _observers = new ConcurrentDictionary<int, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>>();
        }

        /// <summary>
        ///     Creates and adds a new event handler to the observer and subscribes it to the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="channel">
        ///     The channel to listen to.
        /// </param>
        /// <returns>
        ///     Whether the subscribe was successful.
        /// </returns>
        public bool Subscribe(DiscordClient client, DiscordGuild guild, DmHelpKey dmHelpKey)
        {
            if (dmHelpKey.DmHelpResponse is null)
            {
                Log.Error("This dmHelpKey doesn't contain a response! {Id}", dmHelpKey.Id);
                return false;
            }

            try
            {
                if (_observers.TryAdd(dmHelpKey.Id,
                    (sender, e) => SendHelpDmAsync(sender, e, guild, dmHelpKey)))
                {
                    client.MessageCreated += _observers[dmHelpKey.Id];
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.SubscribeToDmHelpErrored(ex.Message, dmHelpKey.Id);
                return false;
            }
        }

        /// <summary>
        ///     Removes a event handler from the observer and unsubscribes it from the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="channelId">
        ///     The id of the channel.
        /// </param>
        /// <returns>
        ///     Whether the unsubscribe was successful.
        /// </returns>
        public bool Unsubscribe(DiscordClient client, DmHelpKey dmHelpKey)
        {
            try
            {
                if (_observers.TryRemove(dmHelpKey.Id, out AsyncEventHandler<DiscordClient, MessageCreateEventArgs>? value))
                {
                    client.MessageCreated -= value;
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.UnsubscribeFromDmHelpErrored(ex.Message, dmHelpKey.Id);
                return false;
            }
        }

        /// <summary>
        ///     Retrieves the current count of active event handlers managed by the observer.
        /// </summary>
        /// <returns>
        ///     The count of event handlers.
        /// </returns>
        public int GetCountOfObservers()
        {
            return _observers.Count;
        }

        private static async Task SendHelpDmAsync(
            DiscordClient sender,
            MessageCreateEventArgs e,
            DiscordGuild guild,
            DmHelpKey dmHelpKey)
        {
            // DMs don't have a Guild!
            if (e.Guild is null || e.Guild.Id != guild.Id || e.Author.IsBot || dmHelpKey.DmHelpResponse is null)
            {
                return;
            }

            if (e.Message is not null)
            {
                var substrings = new List<string>();
                string content = e.Message.Content.ToKeyRepresentation();
                for (int i = 0; i <= content.Length - dmHelpKey.Value.Length; i++)
                {
                    substrings.Add(content.Substring(i, dmHelpKey.Value.Length));
                }

                substrings.Add(content[substrings.Count..]);

                for (int i = 0; i < substrings.Count; i++)
                {
                    if (dmHelpKey.Value.LevenshteinDistance(substrings[i]) <= dmHelpKey.Value.Length / 5)
                    {
                        var member = (DiscordMember)e.Message.Author;
                        await member.SendMessageAsync(dmHelpKey.DmHelpResponse.Text);
                        return;
                    }
                }
            }
        }
    }
}
