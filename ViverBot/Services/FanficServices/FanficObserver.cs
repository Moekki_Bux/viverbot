﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using DSharpPlus;
using DSharpPlus.AsyncEvents;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using System.Collections.Concurrent;
using ViverBot.Handlers;
using ViverBot.Logging;

namespace ViverBot.Services.FanficServices
{
    /// <summary>
    ///     Observer for managing event handlers for the fanfics.
    /// </summary>
    public class FanficObserver
    {
        private readonly ConcurrentDictionary<ulong, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>> _observers;

        public FanficObserver()
        {
            _observers = new ConcurrentDictionary<ulong, AsyncEventHandler<DiscordClient, MessageCreateEventArgs>>();
        }

        /// <summary>
        ///     Creates and adds a new event handler to the observer and subscribes it to the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="thread">
        ///     The thread channel to listen to.
        /// </param>
        /// <returns>
        ///     Whether the subscribe was successful.
        /// </returns>
        public bool Subscribe(DiscordClient client, DiscordThreadChannel thread)
        {
            try
            {
                if (_observers.TryAdd(thread.Id, (sender, e) => FanficHandler.VerifyFanficMessage(sender, e, thread)))
                {
                    client.MessageCreated += _observers[thread.Id];
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.SubscribeToThreadErrored(ex.Message, thread.Id);
                return false;
            }
        }

        /// <summary>
        ///     Removes a event handler from the observer and unsubscribes it from the client.
        /// </summary>
        /// <param name="client">
        ///     The client who owns the event.
        /// </param>
        /// <param name="threadId">
        ///     The id of the thread channel.
        /// </param>
        /// <returns>
        ///     Whether the unsubscribe was successful.
        /// </returns>
        public bool Unsubscribe(DiscordClient client, ulong threadId)
        {
            try
            {
                if (_observers.TryRemove(threadId, out AsyncEventHandler<DiscordClient, MessageCreateEventArgs>? value))
                {
                    client.MessageCreated -= value;
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                ViverLog.UnsubscribeFromThreadErrored(ex.Message, threadId);
                return false;
            }
        }

        /// <summary>
        ///     Retrieves the current count of active event handlers managed by the observer.
        /// </summary>
        /// <returns>
        ///     The count of event handlers.
        /// </returns>
        public int GetCountOfObservers()
        {
            return _observers.Count;
        }
    }
}
