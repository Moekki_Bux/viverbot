﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

using Microsoft.EntityFrameworkCore;
using ViverBot.Context;
using ViverBot.Models;

namespace ViverBot.Services.FanficServices
{
    /// <summary>
    ///     Defines methods to retrieve fanfics and fanficparts from the database.
    /// </summary>
    public interface IFanficService
    {
        /// <summary>
        ///     Get a list of <see cref="Fanfic"/>s.
        /// </summary>
        /// <returns>
        ///     A list containing all fanfics.
        /// </returns>
        Task<List<Fanfic>> GetFanficsAsync();

        /// <summary>
        ///     Get a list of <see cref="Fanfic"/>s.
        /// </summary>
        /// <param name="guildId">
        ///     Filter by guildId.
        /// </param>
        /// <returns>
        ///     A list containing all fanfics based on the chosen filters.
        /// </returns>
        Task<List<Fanfic>> GetFanficsAsync(ulong guildId);

        /// <summary>
        ///     Get a <see cref="Fanfic"/>.
        /// </summary>
        /// <param name="guildId">
        ///     The guildId.
        /// </param>
        /// <param name="threadId">
        ///     The threadId.
        /// </param>
        /// <returns>
        ///     A <see cref="Fanfic"/>.
        /// </returns>
        Task<Fanfic?> GetFanficAsync(ulong guildId, ulong threadId);

        /// <summary>
        ///     Whether there are any active <see cref="Fanfic"/>s.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if any active fanfics are found, otherwise <c>false</c>.
        /// </returns>
        Task<bool> AnyActiveFanficsAsync();

        /// <summary>
        ///     Adds a new <see cref="Fanfic"/> to the database.
        /// </summary>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="authorId">
        ///     The authorId.
        /// </param>
        /// <param name="threadId">
        ///     The id of the newly created thread.
        /// </param>
        /// <param name="guildId">
        ///     The guildId.
        /// </param>
        /// <returns></returns>
        Task AddFanficAsync(string name, ulong authorId, ulong threadId, ulong channelId, ulong guildId);

        /// <summary>
        ///     Updates the database with the <paramref name="fanfic"/>.
        /// </summary>
        /// <param name="fanfic">
        ///     The <see cref="Fanfic"/>.
        /// </param>
        /// <returns></returns>
        Task UpdateFanficAsync(Fanfic fanfic);

        /// <summary>
        ///     Deactivates the <paramref name="fanfic"/>.
        /// </summary>
        /// <param name="fanfic">
        ///     The <see cref="Fanfic"/>.
        /// </param>
        /// <returns></returns>
        Task DeactivateFanficAsync(Fanfic fanfic);

        /// <summary>
        ///     Deletes a <see cref="Fanfic"/> from the database.
        /// </summary>
        /// <param name="name">
        ///     The name.
        /// </param>
        /// <param name="guildId">
        ///     The guildId.
        /// </param>
        /// <param name="threadId">
        ///     The threadId.
        /// </param>
        /// <returns>
        ///     <c>true</c> if the <see cref="Fanfic"/> was deleted, otherwise <c>false</c>.
        /// </returns>
        Task<bool> RemoveFanficAsync(ulong guildId, ulong threadId);
    }

    /// <summary>
    ///     Service to access the database.
    /// </summary>
    public class FanficService : IFanficService
    {
        private readonly ViverContext _dbContext;

        public FanficService(ViverContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Fanfic>> GetFanficsAsync()
        {
            return await _dbContext.Fanfics
                .ToListAsync();
        }

        public async Task<List<Fanfic>> GetFanficsAsync(ulong guildId)
        {
            return await _dbContext.Fanfics
                .Where(f => f.GuildId == guildId)
                .ToListAsync();
        }

        public async Task<Fanfic?> GetFanficAsync(ulong guildId, ulong threadId)
        {
            return await _dbContext.Fanfics
                .Where(f => f.GuildId == guildId &&
                            f.ThreadId == threadId)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> AnyActiveFanficsAsync()
        {
            return await _dbContext.Fanfics
                .Where(f => f.IsActive)
                .AnyAsync();
        }

        public async Task AddFanficAsync(string name, ulong authorId, ulong threadId, ulong channelId, ulong guildId)
        {
            var fanfic = new Fanfic(name, authorId, threadId, channelId, guildId);
            await _dbContext.Fanfics.AddAsync(fanfic);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateFanficAsync(Fanfic fanfic)
        {
            _dbContext.Fanfics.Update(fanfic);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeactivateFanficAsync(Fanfic fanfic)
        {
            fanfic.IsActive = false;
            _dbContext.Fanfics.Update(fanfic);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> RemoveFanficAsync(ulong guildId, ulong threadId)
        {
            var fanfic = await GetFanficAsync(guildId, threadId);
            if (fanfic is not null)
            {
                _dbContext.Fanfics.Remove(fanfic);
                await _dbContext.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
