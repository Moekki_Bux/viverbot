﻿/* ViverBot
 * Copyright (C) 2023  Fynn "Mökki" Hellmund
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ViverBot.Enums
{
    /// <summary>
    ///     Permissions enum for a Discord option picker.
    /// </summary>
    [Flags]
    public enum PermissionOption
    {
        /// <summary>
        ///     Indicates no permissions given.
        /// </summary>
        None = 0,
        /// <summary>
        ///     Indicates all permissions are granted.
        /// </summary>
        All = 1,
        /// <summary>
        ///     Enables full access on a given guild. This also overrides other permissions.
        /// </summary>
        Administrator = 2,
        /// <summary>
        ///     Allows managing channels.
        /// </summary>
        ManageChannels = 4,
        /// <summary>
        ///     Allows managing the guild.
        /// </summary>
        ManageGuild = 8,
        /// <summary>
        ///     Allows managing messages of other users.
        /// </summary>
        ManageMessages = 16,
        /// <summary>
        ///     Allows managing roles in a guild.
        /// </summary>
        ManageRoles = 32,
        /// <summary>
        ///     Allows the user to use application commands.
        /// </summary>
        UseApplicationCommands = 64,
        /// <summary>
        ///     Allows for moderating (Timeout) members in a guild.
        /// </summary>
        ModerateMembers = 128
    }
}
