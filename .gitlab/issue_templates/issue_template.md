## Summary
Give a brief but accurate description of your problem. 
Try to describe here the appearance and origin of the problem. 
This can impact the priority of the issue.

## Description
If possible, please add as much information as you can here. 
This might include runtime, environment, operating system, IDE used, additional exception messages, stack trace, and changes made. 
The more the better.

Especially for exception messages and stack traces it is convenient to write them in code blocks, like this:

```
at ExampleModule.ExampleCommand() at C:\ExanpleBot\ExampleModule.cs: line 162
at ExampleBot.Main(string[])
```

## Steps to reproduce
1. Go to...
2. Click on...
3. Scroll down to...
4. See error...

## Additional context
If you have anything else you'd like to share, you can do so here. 
This can be your own solution attempt, additional information like what you originally wanted to do, or screenshots.
