## Summary
Give a brief but accurate description of your idea. This should not be too long, but should describe the feature clearly.

## Description
This is where you can really go crazy, explain your idea in as much detail as possible, and pitch it as well as you can.

## Additional context
If there is anything else you would like to share with us, you can do so here. 
It can be your inspiration to the idea, an example or screenshots.
