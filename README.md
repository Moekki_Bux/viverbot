# ViverBot

ViverBot was created on demand for the Hellaverse Central Discord server.

ViverBot monitors a channel so that a user can write only one message at a time. If someone sends a second message before another user has had a chance to add their sentence, the bot will remove that message and the creator is reminded to wait.

Additionally, the bot enforces the rule that each message can only contain exactly one sentence. If a message contains more than one sentence, the message is deleted and the user will be reminded to follow the rules.

With ViverBot, users can collaborate on a story and write a plot that is unpredictable at any time. The bot ensures that everyone has a fair chance to contribute to the story.

## Key Features
- Easy to use commands
- Limits users to sending one message at a time in a channel
- Enforces the rule that each message must only contain one sentence
- Automatically removes messages that violate the rules

## Installation
1. Clone the repository.
   ```sh
   git clone https://gitlab.com/Moekki_Bux/viverbot
   ```

2. Publish the project using Visual Studio 2022 with .Net 7 installed

3. Run the project.
   ```sh
   dotnet ViverBot.dll
   ```

4. At the first start the solution will throw an error. The error message will look something like this:
   ```
   IOException: No settings file was found! A new settings file was created at <path>
   ```
5. Open the mentioned file and fill in your bot token and your database connection string.
   > **Caution:** If you use a user token instead of a bot token, you are violating [Discord's Terms of Service](https://discord.com/developers/docs/policies-and-agreements/developer-terms-of-service), which can result in the loss of your account!

6. Run the project again. It should now work fine.

## Support
There will be a Discord server for it, but for now, just text me [Fynn#0037](https://discordapp.com/users/397838318609367041)

## Create a bug report
If you encounter a problem or bug, please file a [bug report](https://gitlab.com/Moekki_Bux/viverbot/-/issues/new?issue[title]=%5BBug%20Report%5D&issuable_template=issue_template). This way you will not only help us, but also all other users of ViverBot.

## Submit a feature request
If you would like to see a new feature or have another idea, please submit a [feature request](https://gitlab.com/Moekki_Bux/viverbot/-/issues/new?issue[title]=%5BFeature%20Request%5D&issuable_template=feature_request). That way everyone can benefit from it.

## Contributing
ViverBot is an open source project, so we always appreciate help.

1. **You want to work on something?**<br>
   Go to the issues and find something for you.

2. **You have an idea for something?**<br>
   Then you can create a feature request and either work on it yourself, or let someone else do it.

## License
ViverBot is licensed under the GNU Affero General Public License v3 and is available free of charge.

This means:
1. anyone can copy, modify and distribute this software.

2. if you distribute the software, you must include the license and a copyright notice.

3. you can use this software privately.

4. you can use this software for commercial purposes.

5. if you publish this software or a modified version, the source code must be made available.

6. if you modify the software, you must indicate the changes to the code.

7. all changes to the code MUST be distributed under the same license, the AGPLv3.

8. this software is provided "as is" without warranty.

9. the author of the software or the license cannot be held liable for any damage caused by the software.

More information about the [license can be found here](https://choosealicense.com/licenses/agpl-3.0/)
